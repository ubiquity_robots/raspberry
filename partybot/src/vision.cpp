// Author: Tuna Oezer (oezer@tunaoezer.com)
//
// Implementation of vision controller.

#include "vision.hpp"

#include "../tools/carl/Timer.h"

#include <fcntl.h>
#include <unistd.h>

#include <iostream>
#include <vector>

#include <opencv2/core/core.hpp>

namespace ubiquity_robots {
namespace partybot {

void myCallback(uint8_t const * const i_frameData, size_t const i_frameSizeBytes, void *i_callbackData)
{
   size_t i=0;
   size_t const frameYCount = i_frameSizeBytes/2;

   for(i=0; i<frameYCount; ++i)
   {
      ((uint8_t*)i_callbackData)[i] = i_frameData[2*i];
   }
}

static const char kWindowName[] = "PartyBot";
static const int kMinFrameSpacingMs = 1;

	Vision::Vision(const Parameters& parameters)
		: cameraHandle_(NULL),
			parameters_(&parameters),
			party_(false),
			person_detector_(parameters) {
		}

	Vision::~Vision() {
		if(cameraHandle_ != NULL) {
			Result cameraResult = camera_destroy(&cameraHandle_);
			if(cameraResult < R_SUCCESS) {
				std::cerr << "Couldn't destroy camera" << std::endl;
			}
		}
	}

	bool Vision::Exists(int const i_cameraIndex) {
		int accessResult = access((std::string("/dev/video")+std::to_string(i_cameraIndex)).c_str(), R_OK);

		return (accessResult != -1);
	}

	void Vision::Init() {
		person_detector_.Load();
		cv::namedWindow(kWindowName, CV_WINDOW_AUTOSIZE);
	}

	bool Vision::Loop() {
		PersonVector persons;
		if(cameraHandle_ == NULL) {
			timer_sleep(0.5);
			if(Vision::Exists(parameters_->camera_index())) {
				this->Set(parameters_->camera_index());
			}
			return true;
		} else {
			cv::Mat frameCarl(parameters_->camera_resolution_y(), parameters_->camera_resolution_x(), CV_8UC1, 255);
			camera_capture_callback(myCallback, frameCarl.ptr(), cameraHandle_);
			person_detector_.Detect(frameCarl, &persons);
			person_detector_.DrawAll(&frameCarl);
			cv::imshow(kWindowName, frameCarl);
			std::cout << "\033[32mVision    : -------------------- FRAME --------------------" << "\033[0m" << std::endl;
		}

		persons_mutex_.lock();
		persons_ = std::move(persons);
		persons_mutex_.unlock();
		
		int key = cv::waitKey(kMinFrameSpacingMs);
		if(key == static_cast<int>('q'))
		{
			return false;
		}

		return true;
	}

	PersonVector Vision::persons() volatile const
	{
		PersonVector outputPersons;

		std::lock_guard<std::mutex> guard(const_cast<std::mutex&>(persons_mutex_));
		outputPersons = const_cast<PersonVector&>(persons_);

		return outputPersons;
	}

	void Vision::Run() {
		bool keepGoing = true;
		while(keepGoing)
		{
			keepGoing = this->Loop();
		}
	}

	bool Vision::Set(int const i_cameraID) {
		Result cameraResult = R_FAILURE;
		CameraHandle *nextHandle = NULL;	

		/***** Open camera *****/
		cameraResult = camera_create(i_cameraID, CAMERA_PIXELFORMAT_YUYV, parameters_->camera_resolution_x(), parameters_->camera_resolution_y(), &nextHandle);
		if(cameraResult < R_SUCCESS) {
			std::cerr << "Unable to open camera '" << i_cameraID << "'" << std::endl;
			return false;
		}

		/***** Start new camera *****/
		cameraResult = camera_start(nextHandle);
		if(cameraResult < R_SUCCESS) {
			std::cerr << "Unable to start camera '" << i_cameraID << "'" << std::endl;
			camera_destroy(&nextHandle);
			return false;
		}

		/***** Detach old camera *****/
		cameraResult = R_FAILURE;
		if(cameraHandle_ != NULL) {
			cameraResult = camera_destroy(&cameraHandle_);
			if(cameraResult < R_SUCCESS) {
				camera_destroy(&nextHandle);
				return false;
			}
		}

		/***** Switch handle *****/
		cameraHandle_ = nextHandle;

		/***** Return *****/
		return true;
	}

}
}
