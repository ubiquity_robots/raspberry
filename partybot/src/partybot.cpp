// Author: Tuna Oezer (oezer@tunaoezer.com)
//
// Partybot tracks and approaches people in a room. It can be used to deliver
// snacks and drinks to people at a party.

#include "behavior.hpp"
#include "joystick.hpp"
#include "robot.hpp"
#include "vision.hpp"
#include "parameters.hpp"

#include <iostream>

int main(int argc, char** argv)
{
	ubiquity_robots::partybot::Parameters const parameters;

	ubiquity_robots::partybot::Robot robot;
	robot.Init();

	ubiquity_robots::partybot::Vision vision(parameters);
	vision.Init();

	ubiquity_robots::partybot::Behavior behavior(&vision, &robot, parameters);
	behavior.Init();

	ubiquity_robots::partybot::Joystick joystick(&behavior, &robot, parameters);
	joystick.Init();

	std::thread visionThread([&](){vision.Run();});
	std::thread behaviorThread([&](){behavior.Run();});
	std::thread joystickThread([&](){joystick.Run();});

	joystickThread.join();
	visionThread.join();
	behaviorThread.join();

	return 0;
}

