// Author: Tuna Oezer (oezer@tunaoezer.com)
//
// Represents a person.

#ifndef UBIQUITY_ROBOTS_PARTYBOT_PERSON_HPP_
#define UBIQUITY_ROBOTS_PARTYBOT_PERSON_HPP_

#include <opencv2/core/core.hpp>

namespace ubiquity_robots {
namespace partybot {

	class Person {
	 public:
		Person(bool face_detected, cv::Rect face,
		bool body_detected, cv::Rect body);

		bool face_detected() const {
			return face_detected_;
		}

		void set_face_detected(bool face_detected) {
			face_detected_ = face_detected;
		}

		const cv::Rect& face() const {
			return face_;
		}

		void set_face(const cv::Rect& face) {
			face_ = face;
		}

		int face_area() const {
			return face_.width * face_.height;
		}

		bool body_detected() const {
			return body_detected_;
		}

		const cv::Rect& body() const {
			return body_;
		}

		int body_area() const {
			return body_.width * body_.height;
		}

		bool InBody(cv::Rect& rect) const;

		int PersonX() const;

	 private:
		bool face_detected_;
		cv::Rect face_;
		bool body_detected_;
		cv::Rect body_;
	};

	typedef std::vector<Person> PersonVector;

}
}
#endif
