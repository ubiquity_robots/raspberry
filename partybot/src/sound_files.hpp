// Author: Tuna Oezer (oezer@tunaoezer.com)
//
// List of sound files.
//
// If a new sound type is added, it must be also added to kSoundFiles.
// Each sound file group must end with a "\0".

namespace ubiquity_robots {
namespace partybot {

	static const char* kRoamSoundFiles[] =
		{ 
			"vivian/A00_hellowav.wav\0",
			"vivian/A01_wouldyoulikecoke.wav\0",
			"vivian/A02_buytheworldcoke.wav\0",

			"\0",	// Must be last.
		};

	static const char* kTrackSoundFiles[] =
		{ 
			"vivian/B01_poursomehappy.wav\0",
			"vivian/B02_youwantcoke.wav\0",
			"vivian/B03_cokehospitality.wav\0",
			"vivian/B04_bestfriendthirst.wav\0",
			"vivian/B05_goodtolastdrop.wav\0",
			"vivian/B06_thirstknowsnoseason.wav\0",
			"vivian/B07_givealittlelove.wav\0",
			"vivian/B08_havecokeandsmile.wav\0",
			"vivian/B09_openhappiness.wav\0",
			"vivian/B10_alwaysrealthing.wav\0",
			"vivian/B11_cokeisit.wav\0",
			"vivian/B12_wherefuncoke.wav\0",
			"vivian/B13_sunshinebirdssing.wav\0",
			"vivian/B14_thingsgobetter.wav\0",
			"vivian/B15_cokeaddslife.wav\0",

			"\0",	// Must be last.
		};

	static const char* kDepartureSoundFiles[] =
		{ 
			"vivian/C00_thankyou.wav\0",
			"vivian/C01_haveniceday.wav\0",

			"\0",	// Must be last.
		};

	static const char* kOfferSoundFiles[] = 
		{
/*
			"vivian/D00_whatsinpants.wav\0",
			"vivian/D01_pleasegrabcans.wav\0",
			"vivian/D02_bottlesarecold.wav\0",
			"vivian/D03_youmakemefizzle.wav\0",
			"vivian/D04_blowbubblesforyou.wav\0",
			"vivian/D05_mobilepartymode.wav\0",
			"vivian/D06_mountainviewlocalway.wav\0",
			"vivian/D07_hackerdojolocalway.wav\0",
*/
			"vivian/D08_pleasetakeacoke.wav\0",
			"vivian/D09_justgrabacoke.wav\0",
			"vivian/D10_takeacoketheyrefree.wav\0",
			//"vivian/D11_takeacoketheyrefiftycents.wav\0",
			//"vivian/D12_takeacoketheyreseventyfivecents.wav\0",
			"vivian/D13_goaheadgrabacoke.wav\0",
			"vivian/D14_goaheadtakeone.wav\0",
			"vivian/D15_placemoneyinbox.wav\0",
			"vivian/D16_takeacokenow.wav\0",
			"vivian/D17_takecokenow.wav\0",
			"\0",
		};

	static const char *kMusicSoundFiles[] =
		{
			"music/A00.wav\0",
			"\0",
		};

	// All sound files.
	static const char** kSoundFiles[] = {
		kRoamSoundFiles,
		kTrackSoundFiles,
		kDepartureSoundFiles,
		kOfferSoundFiles,
		kMusicSoundFiles,
	};
}
}

