// Author: Tuna Oezer (oezer@tunaoezer.com)
//
// Implementation of PersonDetector.

#include "person_detector.hpp"

#include <iostream>

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>

namespace ubiquity_robots {
namespace partybot {

	// Detector parameter specifying how much the i_image size is reduced at
	// each i_image scale.
	const double kDetectorScaleFactor = 1.1;

	// Detector parameter specifying how many neighbors each candidate
	// rectangle should have to retain it.
	const int kDetectorMinNeighbors = 3;
 
	PersonDetector::PersonDetector(const Parameters& parameters)
		: parameters_(&parameters),
			face_detector_(NULL),
			body_detector_(NULL),
			min_face_size_(parameters.min_face_size(), parameters.min_face_size()),
			min_body_size_(parameters.min_body_size(), parameters.min_body_size()) {
	}

	PersonDetector::~PersonDetector() {
		delete face_detector_;
		delete body_detector_;
	}

	void PersonDetector::Load() {
		face_detector_ = new cv::CascadeClassifier();
		if (!face_detector_->load(parameters_->face_detector_model())) {
			std::cout << "Failed to open face detector model " << parameters_->face_detector_model() << std::endl;
		}
		body_detector_ = new cv::CascadeClassifier();
		if (!body_detector_->load(parameters_->body_detector_model())) {
			std::cout << "Failed to open body detector model " << parameters_->body_detector_model() << std::endl;
		}
	}

	void PersonDetector::Detect(cv::Mat const &i_image, PersonVector * const o_persons) {
		faces_.clear();
		bodies_.clear();
		o_persons->clear();

		face_detector_->detectMultiScale(
													i_image,
													faces_,
													kDetectorScaleFactor,
													kDetectorMinNeighbors,
													CV_HAAR_SCALE_IMAGE,
													min_face_size_);

		if(faces_.size() > 0) {
			body_detector_->detectMultiScale(
														i_image,
														bodies_,
														kDetectorScaleFactor,
														kDetectorMinNeighbors,
														CV_HAAR_SCALE_IMAGE,
														min_body_size_);
			Rescale(&bodies_, 1);
		}

		Rescale(&faces_, 1);

		for (std::vector<cv::Rect>::iterator it = bodies_.begin(); it != bodies_.end(); ++it) {
			if (it->y > parameters_->min_target_y() && it->y < parameters_->max_target_y()) {
				o_persons->push_back(Person(false, cv::Rect(), true, *it));
			}
		}

		int num_bodies_detected = o_persons->size();
		for (std::vector<cv::Rect>::iterator it = faces_.begin(); it != faces_.end(); ++it) {
			if (it->y > parameters_->min_target_y() && it->y < parameters_->max_target_y()) {
				bool assigned = false;
				for (int i = 0; i < num_bodies_detected; ++i) {
					Person *person = &o_persons->at(i);
					if (person->face_detected() && person->body_detected()) {
						int current_dx = std::abs(person->face().x - person->body().x);
						int new_dx = std::abs(it->x - person->body().x);
						if (new_dx < current_dx) {
							person->set_face(*it);
						}
					} else {
						if (person->InBody(*it)) {
							person->set_face_detected(true);
							person->set_face(*it);
							assigned = true;
						}
					}
				}
				if (!assigned) {
					o_persons->push_back(Person(true, *it, false, cv::Rect()));
				}
			}
		}
	}

	void PersonDetector::Rescale(std::vector<cv::Rect>* rects, int scale) {
		for (std::vector<cv::Rect>::iterator it = rects->begin(); it != rects->end(); ++it) {
			it->width = it->width * scale;
			it->height = it->height * scale;
			it->x = it->x * scale + (it->width >> 1);
			it->y = it->y * scale + (it->height >> 1);
		}
	}

	void PersonDetector::DrawAll(cv::Mat *io_image) const {
		Draw(faces_, CV_RGB(255, 0, 0), io_image);
		Draw(bodies_, CV_RGB(0, 255, 0), io_image);
	}

	void PersonDetector::Draw(const std::vector<cv::Rect>& rects, cv::Scalar color, cv::Mat *io_image) const {
		for (std::vector<cv::Rect>::const_iterator it = rects.begin(); it != rects.end(); ++it) {
			cv::ellipse(*io_image,
							cv::Point(it->x, it->y),
							cv::Size(it->width >> 1, it->height >> 1),	// axis
							0,															// rotation
							0,															// arc start
							360,														// arc end
							color,													// color
							1,															// line thickness
							8);														// line type
		}
	}
}
}
