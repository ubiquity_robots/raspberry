// Author: Tuna Oezer (oezer@tunaoezer.com)
//
// Global parameters.

#ifndef UBIQUITY_ROBOTS_PARTYBOT_PARAMETERS_HPP_
#define UBIQUITY_ROBOTS_PARTYBOT_PARAMETERS_HPP_

#include <string>

namespace ubiquity_robots {
namespace partybot {

	class Parameters{
	 public:
		Parameters();

		int camera_index() const {
			return 0;
		}

		int camera_resolution_x() const {
			return 160;
		}

		int camera_resolution_y() const {
			return 120;
		}

		const std::string face_detector_model() const {
			return "haarcascades/haarcascade_frontalface_alt2.xml";
		}

		// Minimum face size in pixels.
		int min_face_size() const {
			return 10;
		}

		const std::string body_detector_model() const {
			return "haarcascades/haarcascade_upperbody.xml";
		}

		// Minimum body size in pixels.
		int min_body_size() const {
			return 30;
		}

		// Minimum y coordinate of target center.
		int min_target_y() const {
			return 10;
		}

		// Maximum y coordinate of target center.
		int max_target_y() const {
			return 150;
		}

		size_t min_x_history_size() const {
			return 3;
		}

		size_t max_x_history_size() const {
			return 6;
		}

		double degrees_per_pixel() const {
			return 0.5;
		}

		double distance_factor_face() const {
			return 50.0;
		}

		double distance_factor_body() const {
			return 500.0;
		}

		// Behavior is updated once after this many time units.
		// Time units are defined by master.cpp.
		double behavior_update_time() const {
			return 0.1;
		}

		double sound_play_time() const {
			return 5.0;
		}
	};

}
}
#endif
