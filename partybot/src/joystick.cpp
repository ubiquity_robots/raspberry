// Author: Tuna Oezer (oezer@tunaoezer.com)
//
// Implementation of vision controller.

#include "joystick.hpp"
#include "../tools/carl/Timer.h"

#include <fcntl.h>
#include <unistd.h>
#include <iostream>
#include <vector>

#include <string.h>
#include <opencv2/core/core.hpp>

namespace ubiquity_robots {
namespace partybot {

struct flagmap {
	uint32_t flag;
	char const * const name;
};

#define FLAG(a) {a, #a},

static flagmap const gsc_featureMap[] = {
	FLAG(FF_CONSTANT)
	FLAG(FF_PERIODIC)
		FLAG(FF_SQUARE)
		FLAG(FF_TRIANGLE)
		FLAG(FF_SINE)
		FLAG(FF_SAW_UP)
		FLAG(FF_SAW_DOWN)
	FLAG(FF_RAMP)
	FLAG(FF_SPRING)
	FLAG(FF_FRICTION)
	FLAG(FF_DAMPER)
	FLAG(FF_RUMBLE)
	FLAG(FF_INERTIA)
	FLAG(FF_GAIN)
	FLAG(FF_AUTOCENTER)
};
static size_t const gsc_featureMapCount = sizeof(gsc_featureMap)/sizeof(*gsc_featureMap);

static const char *kJoystickPath = "/dev/input/js0";

	Joystick::Joystick(Behavior volatile * const i_behavior, Robot volatile * const i_robot, const Parameters& parameters)
	:behavior_(i_behavior), js_fd_(-1), parameters_(&parameters), robot_(i_robot)
	{
	}

	Joystick::~Joystick() {
		if(js_fd_ > 0) {
			close(js_fd_);
			js_fd_ = -1;
		}
	}

	bool Joystick::Exists(std::string const &i_joystickFilepath) {
		int accessResult = access(i_joystickFilepath.c_str(), R_OK);

		return (accessResult != -1);
	}

	bool Joystick::Init() {
		return this->Set(kJoystickPath);
	}

	bool Joystick::Loop() {
		if(js_fd_ < 0) {
			timer_sleep(0.5);
			if(Joystick::Exists(kJoystickPath)) {
				this->Set(kJoystickPath);
			}

			return true;
		}

		int bytes = read(js_fd_, &js_event_, sizeof(js_event_));
		if (bytes > 0) {
			js_event_.type &= ~JS_EVENT_INIT;
			if (js_event_.type & JS_EVENT_BUTTON) {
				js_buttons_[js_event_.number] = js_event_.value;
				std::cout << "\033[35m" << "Joystick  : Button " << std::to_string(js_event_.number) << ": " << std::to_string(js_event_.value) << "\033[0m" << std::endl;

				this->OnButton(js_event_.number, js_event_.value == 1);
			}
			if (js_event_.type & JS_EVENT_AXIS) {
				js_axes_[js_event_.number] = js_event_.value;

				std::cout << "\033[35m" << "JS Axis " << std::to_string(js_event_.number) << ": " << std::to_string(js_event_.value) << "\033[0m" << std::endl;

				this->OnAxis(js_event_.number, js_event_.value);
			}
		} else if(bytes < 0 && !(errno == EAGAIN || errno == EWOULDBLOCK)) {
			this->Set();
		}

		/*
		struct input_event shake;
		shake.type = EV_FF;
		shake.code = js_rumble_id_;
		shake.value = 1;
		ssize_t bytesWritten = write(js_fd_, &shake, sizeof(shake));
		if(bytesWritten <= 0) {
			std::cerr << "Error playing rumble" << std::endl;
		}
		*/

		timer_sleep(0.01);
		return true;
	}

	void Joystick::OnAxis(uint8_t const i_axis, int16_t const i_value)
	{	
		if(i_axis == AXIS_TEST_LEFT || i_axis == AXIS_TEST_RIGHT) {
			behavior_->SetMode(Behavior::Mode::MANUAL);

			double const left=(static_cast<double>(js_axes_[AXIS_TEST_LEFT])/static_cast<double>(std::numeric_limits<int16_t>::max())+1.0)/2.0;
			double const right=(static_cast<double>(js_axes_[AXIS_TEST_RIGHT])/static_cast<double>(std::numeric_limits<int16_t>::max())+1.0)/2.0;
			
			double const distance=std::max(left,right)*100;
			double const angle=-90.0*left+90.0*right;
			//std::cout << "LEFT: " << std::to_string(left) << std::endl;
			//std::cout << "RIGHT: " << std::to_string(right) << std::endl;
			robot_->Forward(distance, angle);
		}

		if(behavior_->mode() != Behavior::Mode::MANUAL) {
			return;
		}

		if(i_axis == AXIS_GOTO_X || i_axis == AXIS_GOTO_Y) {
			if(js_axes_[AXIS_GOTO_X] != 0 || js_axes_[AXIS_GOTO_Y] != 0) {
				double x=static_cast<double>(js_axes_[AXIS_GOTO_X])/static_cast<double>(std::numeric_limits<int16_t>::max());
				double y=static_cast<double>(-js_axes_[AXIS_GOTO_Y])/static_cast<double>(std::numeric_limits<int16_t>::max());
				
				double distance=sqrt(x*x+y*y)*300;
				double angle=atan2(x, y)*(180/3.141592654);
				if(y>=0.0) {
					robot_->GoTo(distance, angle);
				} else {
					robot_->Stop();
				}
			} else {
				robot_->Stop();
			}
		}

		if(i_axis == AXIS_MANUAL_X || i_axis == AXIS_MANUAL_Y) {
			if(js_axes_[AXIS_MANUAL_X] != 0 || js_axes_[AXIS_MANUAL_Y] != 0) {
				double x=static_cast<double>(js_axes_[AXIS_MANUAL_X])/static_cast<double>(std::numeric_limits<int16_t>::max());
				double y=static_cast<double>(-js_axes_[AXIS_MANUAL_Y])/static_cast<double>(std::numeric_limits<int16_t>::max());
				
				double distance=sqrt(x*x+y*y)*100;
				double angle=atan2(x, y)*(180/3.141592654);
				if(y>=0.0) {
					robot_->Forward(distance, angle);
				} else {
					robot_->Reverse(distance, angle);
				}
			} else {
				robot_->Stop();
			}
		}


	}

	void Joystick::OnButton(uint8_t const i_button, bool const i_down)
	{
		if(i_button == BUTTON_MANUAL_TOGGLE_0 || i_button == BUTTON_MANUAL_TOGGLE_1) {
			if(i_down) {
				behavior_->SetMode(Behavior::Mode::MANUAL);
				robot_->Stop();
			} else {
				robot_->Stop();
				behavior_->SetMode(Behavior::Mode::ROAM1);
			}
		} else if(i_button == BUTTON_MANUAL_ENABLE_0 || i_button == BUTTON_MANUAL_ENABLE_1) {
			if(i_down) {
				behavior_->SetMode(Behavior::Mode::MANUAL);
				robot_->Stop();
			}
		} else if(i_button == BUTTON_MANUAL_DISABLE_0 || i_button == BUTTON_MANUAL_DISABLE_1) {
			if(i_down) {
				robot_->Stop();
				behavior_->SetMode(Behavior::Mode::ROAM1);
			}
		} else if(i_button == BUTTON_PARTY) {
			if(i_down) {
				robot_->Stop();
				behavior_->SetMode(Behavior::Mode::PARTY1);
				std::cout << "Party!!!" << std::endl;
			}
		} else if(i_button == BUTTON_VOICE_ROAM) {
			if(i_down) {
				sound_player_.PlaySound(SoundPlayer::ROAM_SOUND);
			}
		} else if(i_button == BUTTON_VOICE_TRACK) {
			if(i_down) {
				sound_player_.PlaySound(SoundPlayer::TRACK_SOUND);
			}
		} else if(i_button == BUTTON_VOICE_OFFER) {
			if(i_down) {
				sound_player_.PlaySound(SoundPlayer::OFFER_SOUND);
			}
		} else if(i_button == BUTTON_VOICE_DEPARTURE) {
			if(i_down) {
				sound_player_.PlaySound(SoundPlayer::DEPARTURE_SOUND);
			}
		} else if(i_button == BUTTON_DRIVE_STOP_0 || i_button == BUTTON_DRIVE_STOP_1) {
			if(i_down) {
				robot_->Stop();
			}
		} else if(i_button == BUTTON_EXIT) {
			if(i_down) {
				exit(0);
			}
		}

		if(js_buttons_[BUTTON_DRIVE_BACKWARD]) {
			robot_->Reverse(50, 0);
		} else if(js_buttons_[BUTTON_DRIVE_FORWARD]) {
			robot_->GoTo(300, 0);
		} else if(js_buttons_[BUTTON_DRIVE_LEFT]) {
			robot_->GoTo(300, -90);
		} else if(js_buttons_[BUTTON_DRIVE_RIGHT]) {
			robot_->GoTo(300, 90);
		}
	}

	void Joystick::Run() {
		bool keepGoing = true;
		while(keepGoing)
		{
			keepGoing = this->Loop();
			if(!keepGoing) {
				exit(0);
			}
		}
	}

	bool Joystick::Set(std::string const &i_joystickFilepath) {
		if(i_joystickFilepath.empty()) {
			if(js_fd_ > 0) {
				close(js_fd_);
				js_fd_ = -1;
			}
			return true;
		}

		/***** Open Joystick *****/
		int nextFD = open(i_joystickFilepath.c_str(), O_RDWR | O_NONBLOCK);
		if(nextFD < 0) {
			std::cerr << "Unable to open joystick '" << i_joystickFilepath << "'" << std::endl;
			return false;
		}
		
		/***** Close old joystick *****/
		if(js_fd_ >= 0) {
			close(js_fd_);
			js_fd_ = -1;
		}

		/***** Set things for new joystick *****/
		js_fd_ = nextFD;
		ioctl(js_fd_, JSIOCGNAME(js_name_length_max_), js_name_);
		ioctl(js_fd_, JSIOCGVERSION, &js_version_);
		ioctl(js_fd_, JSIOCGAXES, &js_axis_count_);
		ioctl(js_fd_, JSIOCGBUTTONS, &js_button_count_);
		ioctl(js_fd_, EVIOCGBIT(EV_FF, sizeof(js_features_)), &js_features_);
		js_axes_.resize(js_axis_count_);
		js_buttons_.resize(js_button_count_);

		/***** Map buttons *****/
		if(!strcasecmp(js_name_, "Saitek PLC Saitek P2600 Rumble Force Pad")) {
			AXIS_GOTO_X=4;
			AXIS_GOTO_Y=5;
			AXIS_MANUAL_X=0;
			AXIS_MANUAL_Y=1;
			AXIS_TEST_LEFT=-1;
			AXIS_TEST_RIGHT=-1;
			BUTTON_DRIVE_BACKWARD=-1;
			BUTTON_DRIVE_FORWARD=-1;
			BUTTON_DRIVE_LEFT=-1;
			BUTTON_DRIVE_RIGHT=-1;
			BUTTON_DRIVE_STOP_0=-1;
			BUTTON_DRIVE_STOP_1=-1;
			BUTTON_EXIT=8;
			BUTTON_MANUAL_TOGGLE_0=6;
			BUTTON_MANUAL_TOGGLE_1=7;
			BUTTON_MANUAL_ENABLE_0=4;
			BUTTON_MANUAL_ENABLE_1=5;
			BUTTON_MANUAL_DISABLE_0=9;
			BUTTON_MANUAL_DISABLE_1=-1;
			BUTTON_PARTY=-1;
			BUTTON_VOICE_ROAM=1;
			BUTTON_VOICE_TRACK=2;
			BUTTON_VOICE_OFFER=3;
			BUTTON_VOICE_DEPARTURE=0;
		} else if(!strncasecmp(js_name_, "Xbox", 4)) {
			AXIS_GOTO_X=-1;
			AXIS_GOTO_Y=-1;
			AXIS_MANUAL_X=0;
			AXIS_MANUAL_Y=1;
			AXIS_TEST_LEFT=5;
			AXIS_TEST_RIGHT=2;
			BUTTON_DRIVE_BACKWARD=14;
			BUTTON_DRIVE_FORWARD=13;
			BUTTON_DRIVE_LEFT=11;
			BUTTON_DRIVE_RIGHT=12;
			BUTTON_DRIVE_STOP_0=10;
			BUTTON_DRIVE_STOP_1=9;
			BUTTON_EXIT=8;
			BUTTON_MANUAL_TOGGLE_0=-1;
			BUTTON_MANUAL_TOGGLE_1=-1;
			BUTTON_MANUAL_ENABLE_0=4;
			BUTTON_MANUAL_ENABLE_1=5;
			BUTTON_MANUAL_DISABLE_0=7;
			BUTTON_MANUAL_DISABLE_1=-1;
			BUTTON_PARTY=6;
			BUTTON_VOICE_ROAM=0;
			BUTTON_VOICE_TRACK=1;
			BUTTON_VOICE_OFFER=3;
			BUTTON_VOICE_DEPARTURE=2;
		} else {
			AXIS_GOTO_X=-1;
			AXIS_GOTO_Y=-1;
			AXIS_MANUAL_X=0;
			AXIS_MANUAL_Y=1;
			AXIS_TEST_LEFT=-1;
			AXIS_TEST_RIGHT=-1;
			BUTTON_DRIVE_BACKWARD=-1,
			BUTTON_DRIVE_FORWARD=-1,
			BUTTON_DRIVE_LEFT=-1,
			BUTTON_DRIVE_RIGHT=-1,
			BUTTON_DRIVE_STOP_0=-1;
			BUTTON_DRIVE_STOP_1=-1;
			BUTTON_EXIT=-1;
			BUTTON_MANUAL_TOGGLE_0=0;
			BUTTON_MANUAL_TOGGLE_1=0;
			BUTTON_MANUAL_ENABLE_0=1;
			BUTTON_MANUAL_ENABLE_1=1;
			BUTTON_MANUAL_DISABLE_0=2;
			BUTTON_MANUAL_DISABLE_1=3;
			BUTTON_PARTY=-1;
			BUTTON_VOICE_ROAM=4;
			BUTTON_VOICE_TRACK=5;
			BUTTON_VOICE_OFFER=6;
			BUTTON_VOICE_DEPARTURE=7;
		}


		/***** Flush any events *****/
		int bytes=0;
		do {
			bytes = read(js_fd_, &js_event_, sizeof(js_event_));
		} while(bytes > 0);
		memset(&js_event_, 0, sizeof(js_event_));

		/***** Print new joystick information *****/
		std::cout << "   Name: " << js_name_ << std::endl;
		std::cout << "Version: " << js_version_ << std::endl;
		std::cout << "   Axes: " << js_axis_count_ << std::endl;
		std::cout << "Buttons: " << js_button_count_ << std::endl;
		std::cout << "Features: ";
		for(size_t i=0; i<gsc_featureMapCount; ++i) {
			if(js_features_ & gsc_featureMap[i].flag) {
				std::cout << gsc_featureMap[i].name << " ";
			}
		}
		std::cout << std::endl;

		/***** Upload effect *****/
		/*
		struct ff_effect effect;
		effect.type = FF_RUMBLE;
		effect.id = -1;
		effect.replay.length = 5000;
		effect.replay.delay = 0;
		effect.u.rumble.strong_magnitude = 0xc000;
		effect.u.rumble.weak_magnitude = 0;
		if(ioctl(js_fd_, EVIOCSFF, &effect)==-1) {
			std::cerr << "Error uploading effect to joystick." << std::endl;
			js_rumble_id_ = -1;
		} else {
			js_rumble_id_ = effect.id;
		}
		*/

		return true;
	}

}
}

