// Author: Tuna Oezer (oezer@tunaoezer.com)
//
// Implementation of Target.

#include <cmath>

#include "target.hpp"

namespace ubiquity_robots {
namespace partybot {

	Target::Target(const Parameters& parameters)
		: parameters_(&parameters) {
	}

	bool Target::Active() {
		return x_history_.size() >= parameters_->min_x_history_size();
	}

	int Target::AverageX() {
		int sum = 0;
		for (std::list<int>::const_iterator it = x_history_.begin(); it != x_history_.end(); ++it) {
			sum += *it;
		}
		return sum / static_cast<int>(x_history_.size());
	}

	void Target::Track(PersonVector const &persons) {
		PersonVector::const_iterator best = persons.end();
		int tx = 0;
		if (Active()) {
			tx = AverageX();
		}
		int best_dx = std::numeric_limits<int>::max();
		for (PersonVector::const_iterator it = persons.begin(); it != persons.end(); ++it) {
			if (best != persons.end()) {
			 best = it;
			 best_dx = std::abs(tx - best->PersonX());
			} else {
			 int dx = tx - it->PersonX();
			 if (dx < 0) dx = -dx;
			 if (dx < best_dx) {
				 best_dx = dx;
				 best = it;
			 }
			}
		}
		if (best != persons.end()) {
			x_history_.push_back(best->PersonX());
			if (best->face_detected()) {
				face_detected_ = true;
				last_size_ = best->face_area();
			} else {
				face_detected_ = false;
				last_size_ = best->body_area();
			}
			if (x_history_.size() > parameters_->max_x_history_size()) {
				x_history_.pop_front();
			}
		} else {
			if (!x_history_.empty()) x_history_.pop_front();
		}
	}

	int Target::Angle() {
		double angle = (AverageX() - (parameters_->camera_resolution_x()/2.0)) * parameters_->degrees_per_pixel();
		if (angle < 0.0) angle += 360;
		return static_cast<int>(angle) % 360;
	}

	int Target::Distance() {
		double factor = face_detected_ ?  parameters_->distance_factor_face() : parameters_->distance_factor_body();
		double distance = sqrt(last_size_);
		if (distance > 0.0) distance = factor / distance;
		else distance = 0.0;
		return static_cast<int>(distance * 100);
	}
}
}
