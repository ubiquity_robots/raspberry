// Author: Tuna Oezer (oezer@tunaoezer.com)
//
// Implementation of Behavior.

#include "behavior.hpp"

#include <iostream>

#include "person.hpp"
#include "robot.hpp"

#define DANCE_STOP dist=0.0; angle=0.0;
#define DANCE_LEFT dist=100.0; angle=270.0;
#define DANCE_RIGHT dist=100.0; angle=90.0;
#define DANCE_FORWARD dist=100.0; angle=0.0;
#define DANCE_JERK_LEFT		if(t_mode_remainder <= 500) { dist = 300; angle = 270; }
#define DANCE_JERK_RIGHT	if(t_mode_remainder <= 500) { dist=300; angle=90; }
#define DANCE_JERK_FORWARD	if(t_mode_remainder <= 500) { dist = 300; if(t_lap_int & 1) { angle = 90; } else { angle = 270; } }

namespace ubiquity_robots {
namespace partybot {

	static const int kDistMin = 100;
	static const int kMove1Max = 100;
	static const int kMove2Max = 200;
	static const int kTurnMax = 60;
	static const int kTurnMoveMax = 100;

	Behavior::Behavior(Vision volatile const * const vision, Robot volatile * const robot, const Parameters& parameters)
		: last_angle_(0),
			last_dist_(0),
			mode_(Mode::ROAM1),
			parameters_(&parameters),
			robot_(robot),
			t_last_behavior_update_(0),
			t_last_sound_play_time_(0),
			t_mode_start_(0.0),
			target_(parameters),
			vision_(vision)
	{
		srand(time(NULL));
	}

	void Behavior::Init() {
		timer_start(&timer_);
	}

	void Behavior::Loop() {
		double const t_current = timer_total_seconds(&timer_);
		double const t_since_last_behavior_update = t_current - t_last_behavior_update_;
		double const t_behavior_update = parameters_->behavior_update_time();
		if(t_since_last_behavior_update < t_behavior_update) {
			timer_sleep((t_behavior_update-t_since_last_behavior_update)*0.9);
			return;
		}
		t_last_behavior_update_ = t_current;

		if(mode_ == Behavior::Mode::MANUAL) {
			return;
		}

		/***** Check if tracking target *****/
		target_.Track(vision_->persons());
		bool tracking_now = false;
		if (target_.Active()) {
			tracking_now = true;

			if(target_.Distance() < kDistMin) {
				this->SetMode(Mode::OFFER1);
			} else {
				this->SetMode(Mode::TRACK1);
			}
		}

		std::lock_guard<std::mutex> guard(const_cast<std::mutex&>(mode_mutex_));
		int dist = 0;
		int angle = 0;
		switch (mode_) {
			case Mode::PARTY1: {
				if(last_mode_ != Mode::PARTY1) {
					sound_player_.PlaySound(SoundPlayer::MUSIC_SOUND);
					std::cout << "**************************************************" << std::endl;
					std::cout << "**************************************************" << std::endl;
					std::cout << "********************* PARTY **********************" << std::endl;
					std::cout << "**************************************************" << std::endl;
					std::cout << "**************************************************" << std::endl;
				}
				double const t_mode = t_current - t_mode_start_;
				if(t_mode < 272.0)
				{
					int const t_mode_remainder = static_cast<int>((t_mode-floor(t_mode))*1000);
					double t_lap = t_mode;
					int const t_lap_int = static_cast<unsigned int>(t_mode);

					if(t_lap < 8.5) {
						DANCE_STOP;
					} else if(t_lap < 17.0) {
						DANCE_FORWARD;
					} else if(t_lap < 24.0) {
						DANCE_RIGHT;
					} else if(t_lap < 33.0) {
						DANCE_LEFT;
					} else if(t_lap < 41.0) {
						DANCE_RIGHT;
					} else if(t_lap < 49.0) {
						DANCE_JERK_LEFT;
					} else if(t_lap < 56.0) {
						DANCE_JERK_RIGHT;
					} else if(t_lap < 64.0) {
						DANCE_LEFT;
					} else if(t_lap < 72) {
						DANCE_RIGHT;
					} else if(t_lap < 73.5) {
						DANCE_FORWARD;
					} else if(t_lap < 74) {
						DANCE_STOP;
					} else if(t_lap < 76) {
						DANCE_FORWARD;
					} else if(t_lap < 80) {
						DANCE_STOP;
					} else if(t_lap < 87) {
						DANCE_FORWARD;
					} else if(t_lap < 95) {
						DANCE_RIGHT;
					} else if(t_lap < 103) {
						DANCE_LEFT;
					} else if(t_lap < 116.5) {
						DANCE_JERK_FORWARD;
					} else if(t_lap < 118.0) {
						DANCE_STOP;
					} else if(t_lap < 126.5) {
						DANCE_RIGHT;
					} else if(t_lap < 133.0) {
						DANCE_LEFT;
					} else if(t_lap < 140.0) {
						DANCE_FORWARD;
					} else if(t_lap < 142.0) {
						DANCE_STOP;
					} else if(t_lap < 148.5) {
						DANCE_FORWARD;
					} else if(t_lap < 156.5) {
						DANCE_RIGHT;
					} else if(t_lap < 165.0) {
						DANCE_JERK_FORWARD;
					} else if(t_lap < 172.0) {
						DANCE_STOP;
					} else if(t_lap < 180.0) {
						DANCE_FORWARD;
					} else if(t_lap < 184.5) {
						DANCE_JERK_RIGHT;
					} else if(t_lap < 188.0) {
						DANCE_JERK_LEFT;
					} else if(t_lap < 203.0) {
						DANCE_JERK_FORWARD;
					} else if(t_lap < 211.5) {
						DANCE_LEFT;
					} else if(t_lap < 219.0) {
						DANCE_RIGHT;
					} else if(t_lap < 227.0) {
						DANCE_LEFT;
					} else if(t_lap < 234.0) {
						DANCE_RIGHT;
					} else if(t_lap < 241.5) {
						DANCE_JERK_FORWARD;
					} else if(t_lap < 250.0) {
						DANCE_LEFT;
					} else if(t_lap < 256.0) {
						DANCE_RIGHT;
					} else if(t_lap < 264.0) {
						DANCE_JERK_FORWARD;
					} else if(t_lap < 268.0) {
						DANCE_STOP;
					} else if(t_lap < 271.0) {
						DANCE_FORWARD;
					} else {
						DANCE_STOP;
						this->SetModeInternal(Mode::ROAM1);
					}
				} else {
					this->SetModeInternal(Mode::ROAM1);
				}
				break;
			}
			case Mode::TRACK1:
				if(tracking_now) {
					dist = target_.Distance();
					angle = target_.Angle();
				} else {
					mode_flags_ |= ModeFlag::TARGET_LOST;
					this->SetModeInternal(Mode::ROAM1);
				}
				break;
			case Mode::ROAM1:
				angle = 0;
				dist = 0;
				if((t_current - t_mode_start_) < 16.0) {
					if(static_cast<unsigned int>(t_current - t_mode_start_) & 0x02) {
						angle = 315;
						dist = 100;
					}
				}
				else {
					this->SetModeInternal(Mode::ROAM2);
				}
				break;
			case Mode::ROAM2:
				angle = 0;
				dist = 100;
				if((t_current - t_mode_start_) >= 4.0) {
					this->SetModeInternal(Mode::ROAM1);
				}
				break;
			case Mode::OFFER1:
				angle = 0;
				dist = 0;
				if(!tracking_now || ((t_current-t_mode_start_) >= 30.0)) {
					mode_flags_ |= ModeFlag::TARGET_LOST;
					this->SetModeInternal(Mode::ROAM1);
				}
				break;
			default:
				angle = 0;
				dist = 0;
				this->SetModeInternal(Mode::ROAM1);
				break;
		}
		std::cout << "\033[33mBehavior  : mode " << this->ModeStr(mode_) << " goto " << dist << ", " << angle << "\033[0m" << std::endl;
		robot_->GoTo(dist, angle);

		// Sound play
		if (t_current - t_last_sound_play_time_ > parameters_->sound_play_time()) {
			if(mode_ == Mode::OFFER1 || mode_ == Mode::TRACK1) {
				switch (mode_) {
					case Mode::OFFER1:
						sound_player_.PlaySound(SoundPlayer::OFFER_SOUND);
						t_last_sound_play_time_ = t_current;
						break;
					case Mode::TRACK1:
						sound_player_.PlaySound(SoundPlayer::TRACK_SOUND);
						t_last_sound_play_time_ = t_current;
						break;
					default: /*sound_player_.PlaySound(SoundPlayer::ROAM_SOUND);*/ break;
				}
			} else if (mode_flags_ & ModeFlag::TARGET_LOST) {
				sound_player_.PlaySound(SoundPlayer::DEPARTURE_SOUND);
				mode_flags_ &= (~ModeFlag::TARGET_LOST);
				t_last_sound_play_time_ = t_current;
			}

		}
	
		last_mode_ = mode_;
		last_dist_ = dist;
		last_angle_ = angle;
	}

	void Behavior::Run() {
		while(true)
		{
			this->Loop();
		}
	}

	void Behavior::SetMode(Mode const i_mode) volatile {
		std::lock_guard<std::mutex> guard(const_cast<std::mutex&>(mode_mutex_));
		const_cast<Behavior*>(this)->SetModeInternal(i_mode);
	}

	void Behavior::SetModeInternal(Mode const i_mode) {
		if(mode_ == i_mode) {
			return;
		}

		mode_ = i_mode;
		t_mode_start_ = timer_total_seconds(&timer_);
	}
		

	std::string Behavior::ModeStr(Mode const i_mode) {
		switch(i_mode) {
			case Mode::MANUAL:	return "MANUAL";
			case Mode::PARTY1:	return "PARTY1";
			case Mode::OFFER1: 	return "OFFER1";
			case Mode::ROAM1:	return "ROAM1";
			case Mode::ROAM2:	return "ROAM2";
			case Mode::TRACK1:	return "TRACK1";
			default:		return "UNKNOWN";
		}
	}

	Behavior::Mode Behavior::mode() volatile const
	{
		std::lock_guard<std::mutex> guard(const_cast<std::mutex&>(mode_mutex_));
		return mode_;
	}

}
}
