// Author: Tuna Oezer (oezer@tunaoezer.com)
//
// Implementation of SoundPlayer.

#include "sound_player.hpp"

#include <unistd.h>
#include <cstdio>
#include <cstdlib>
#include <iostream>

#include "sound_files.hpp"

namespace ubiquity_robots {
namespace partybot {

	SoundPlayer::SoundPlayer() {
		sound_indices_.resize(NUM_SOUND_TYPES, 0);
		num_sound_files_.resize(NUM_SOUND_TYPES, 0);
		for (int i = 0; i < NUM_SOUND_TYPES; ++i) {
			num_sound_files_[i] = NumSoundFiles(static_cast<SoundTypes>(i));
		}
	}

	// static
	int SoundPlayer::NumSoundFiles(SoundTypes sound_type) {
		const char** sound_files = kSoundFiles[sound_type];
		int count = 0;
		while (sound_files[count][0] != '\0') count++;
		return count;
	}

	void SoundPlayer::PlaySound(const char* filename) {
		pid_t id = fork();
		if (id == 0) {
			std::cout << "\033[36m" << "SoundPlay : Playing " << filename << "\033[0m" << std::endl;
			char command[256];
			sprintf(command, "aplay -c 1 -q -t wav %s", filename);
			int systemResult=system(command);
			if(systemResult != 0) {
				std::cerr << "Error playing " << filename << std::endl;
			}
			exit(0);
		} else if (id < 0) {
			std::cout << "Failed to play sound." << std::endl;
		}
	}

	void SoundPlayer::PlaySound(SoundTypes sound_type) {
		if (num_sound_files_[sound_type] == 0) return;
		sound_indices_[sound_type]++;
		if (sound_indices_[sound_type] >= num_sound_files_[sound_type]) {
			sound_indices_[sound_type] = 0;
		}
		PlaySound(kSoundFiles[sound_type][sound_indices_[sound_type]]);
	}

}
}
