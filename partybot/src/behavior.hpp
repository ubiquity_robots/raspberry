// Author: Tuna Oezer (oezer@tunaoezer.com)
//
// Defines the robot behavior.

#ifndef UBIQUITY_ROBOTS_PARTYBOT_BEHAVIOR_HPP_
#define UBIQUITY_ROBOTS_PARTYBOT_BEHAVIOR_HPP_

#include "vision.hpp"
#include "parameters.hpp"
#include "target.hpp"
#include "sound_player.hpp"

#include "../tools/carl/Timer.h"

#include <thread>

namespace ubiquity_robots {
namespace partybot {

	class Robot;

	class Behavior : protected std::thread {
	 public:
		enum class Mode {
			MANUAL,
			PARTY1,
			ROAM1,
			ROAM2,
			TRACK1,
			OFFER1
		};

		Behavior(Vision volatile const * const vision, Robot volatile * const robot, const Parameters& parameters);

		void Init();
		void Run();

		Behavior::Mode mode() volatile const;
		void SetMode(Mode const i_mode) volatile;

	 protected:
		void Loop();

	 private:
		enum ModeFlag : unsigned int {
			TARGET_LOST=(1<<0)
		};
		static std::string ModeStr(Mode const i_mode);
		void SetModeInternal(Mode const i_mode);

		int last_angle_;
		int last_dist_;
		Mode last_mode_;
		Mode mode_;
		unsigned int mode_flags_;
		std::mutex mode_mutex_;
		const Parameters* parameters_;
		Robot volatile * robot_;
		SoundPlayer sound_player_;
		double t_last_behavior_update_;
		double t_last_sound_play_time_;
		double t_mode_start_;
		Target target_;
		bool target_lost_;
		Timer timer_;
		Vision volatile const * vision_;
	};

}
}

#endif
