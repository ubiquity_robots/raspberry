// Author: Tuna Oezer (oezer@tunaoezer.com)
//
// Represents the currently tracked target.

#ifndef UBIQUITY_ROBOTS_PARTYBOT_TARGET_HPP_
#define UBIQUITY_ROBOTS_PARTYBOT_TARGET_HPP_

#include <list>

#include "person.hpp"
#include "parameters.hpp"

namespace ubiquity_robots {
namespace partybot {

	class Target {
	 public:
		Target(const Parameters& parameters);

		bool Active();

		int AverageX();

		int Angle();

		int Distance();

		void Track(PersonVector const &persons);

	 private:
		const Parameters* parameters_;
		std::list<int> x_history_;
		int last_size_;
		bool face_detected_;
	};

}
}
#endif
