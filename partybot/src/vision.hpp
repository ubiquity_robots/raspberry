// Author: Tuna Oezer (oezer@tunaoezer.com)
//
// Vision controller.

#ifndef UBIQUITY_ROBOTS_PARTYBOT_VISION_HPP_
#define UBIQUITY_ROBOTS_PARTYBOT_VISION_HPP_

#include <opencv2/highgui/highgui.hpp>
#include <mutex>
#include <thread>

#include "parameters.hpp"
#include "person_detector.hpp"
#include "robot.hpp"

#include "../tools/carl/Camera.h"

namespace ubiquity_robots {
namespace partybot {

	class Vision : protected std::thread {
	 public:
		explicit Vision(const Parameters& parameters);

		// Initializes the Vision.
		void Init();
		void Run();

		PersonVector persons() volatile const;

		~Vision();

	 protected:
		// Runs one iteration of the Vision loop.
		static bool Exists(int const i_cameraID);
		bool Loop();

		bool Set(int const i_cameraID);

	 private:
		CameraHandle *cameraHandle_;

		Parameters const * const parameters_;

		bool party_;
		PersonDetector person_detector_;
		PersonVector persons_;
		std::mutex persons_mutex_;
		Robot robot_;
	};

}
}
#endif
