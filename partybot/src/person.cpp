// Author: Tuna Oezer (oezer@tunaoezer.com)
//
// Implementation of a Person.

#include "person.hpp"

namespace ubiquity_robots {
namespace partybot {

	Person::Person(bool face_detected, cv::Rect face,
		bool body_detected, cv::Rect body)
		: face_detected_(face_detected),
			face_(face),
			body_detected_(body_detected),
			body_(body) {
	}

	bool Person::InBody(cv::Rect& rect) const {
		if (!body_detected_) return false;
		int half_body_w = body_.width >> 1;
		int half_body_h = body_.height >> 1;
		int half_rect_w = rect.width >> 1;
		int half_rect_h = rect.height >> 1;
		return
			(body_.x - half_body_w) < (rect.x - half_rect_w) &&
			(body_.x + half_body_w) > (rect.x + half_rect_w) &&
			(body_.y - half_body_h) < (rect.y - half_rect_h) &&
			(body_.y + half_body_h) > (rect.y + half_rect_h);
	}

	int Person::PersonX() const {
		if (face_detected_) return face_.x;
		else return body_.x;
	}
}
}
