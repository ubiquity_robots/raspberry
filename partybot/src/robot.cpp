// Author: Tuna Oezer (oezer@tunaoezer.com)
//
// Implementation of Robot.

#include "robot.hpp"

#include <iostream>
#include <string.h>
#include <unistd.h>

static int kArduinoID = 0;

namespace ubiquity_robots {
namespace partybot {

	Robot::Robot()
		: serial_(NULL) {
	}

	Robot::~Robot() {
		if (serial_ == NULL) {
			return;
		}

		this->Stop();

		Result result = serial_destroy(&serial_);
		if(result < R_SUCCESS) {
			std::cerr << "Failed to destroy serial." << std::endl;
		}
	}

	bool Robot::Exists(int const i_arduinoID) {
		int accessResult = access((std::string("/dev/ttyUSB")+std::to_string(i_arduinoID)).c_str(), R_OK);

		return (accessResult != -1);
	}

	bool Robot::Init() {
		return this->Set(kArduinoID);
	}

	bool Robot::Set(int const i_arduinoID) {
		Result result = R_FAILURE;

		/***** Close old serial port *****/
		if(serial_ != NULL) {
			result = serial_destroy(&serial_);
			if(result < R_SUCCESS) {
				std::cerr << "Failed to destroy serial." << std::endl;
				return false;
			}
		}

		/***** Open new serial port *****/
		result = serial_create(i_arduinoID, SERIAL_BAUDRATE_115200, SERIAL_MODE_ARDUINO, &serial_);
		if(result < R_SUCCESS) {
			std::cerr << "Failed to initialize serial port." << std::endl;
			return false;
		}

		/***** Wait for Arduino response *****/
		std::cout << "Waiting for Arduino startup..." << std::endl;
		sleep(10);

		/***** Check for Arduino *****/
		uint8_t read_buffer[7] = { 0 };
		result = serial_read(6, read_buffer, NULL, serial_);
		if(result < R_SUCCESS) {
			std::cerr << "Failed to read ready." << std::endl;
			serial_destroy(&serial_);
			return false;
		}

		/***** Print Arduino response *****/
		std::cout << "Arduino: " << read_buffer << std::endl;
		return true;
	}

	void Robot::Forward(int const i_distance, int const i_angle) volatile {
		std::lock_guard<std::mutex> guard(const_cast<std::mutex&>(serial_mutex_));
		if(serial_ == NULL && Robot::Exists(kArduinoID)) {
			const_cast<Robot*>(this)->Set(kArduinoID);
		}

		/***** Clamp distance *****/
		int distance = i_distance;
		if(distance < 0) {
			distance = 0;
		} else if(distance > 999) {
			distance = 999;
		}

		/***** Clamp angle *****/
		int angle = i_angle;
		while(angle < 0) {
			angle += 360;
		}
		while(angle >= 360) {
			angle -= 360;
		}

		/***** Generate string *****/
		uint8_t buffer[8];
		int bufferCount = snprintf(reinterpret_cast<char *>(buffer), sizeof(buffer), "F%03d%03d", i_distance, i_angle);
		if(bufferCount <= 0) {
			std::cerr << "Unable to format serial data." << std::endl;
		}

		if (serial_ != NULL) {
			Result result = serial_write(bufferCount, buffer, NULL, serial_);
			if(result < R_SUCCESS) {
				std::cerr << "Unable to send data." << std::endl;
				serial_destroy(const_cast<Serial**>(&serial_));
			}
		}
		std::cout << "\033[31m" << "Robot     : " << buffer << "\033[0m" << std::endl;
	}

	void Robot::GoTo(int const i_distance, int const i_angle) volatile {
		std::lock_guard<std::mutex> guard(const_cast<std::mutex&>(serial_mutex_));
		if(serial_ == NULL && Robot::Exists(kArduinoID)) {
			const_cast<Robot*>(this)->Set(kArduinoID);
		}

		/***** Clamp distance *****/
		int distance = i_distance;
		if(distance < 0) {
			distance = 0;
		} else if(distance > 999) {
			distance = 999;
		}

		/***** Clamp angle *****/
		int angle = i_angle;
		while(angle < 0) {
			angle += 360;
		}
		while(angle >= 360) {
			angle -= 360;
		}

		/***** Compose command *****/
		uint8_t buffer[8];
		int bufferCount = snprintf(reinterpret_cast<char *>(buffer), sizeof(buffer), "G%03d%03d", i_distance, i_angle);
		if(bufferCount <= 0) {
			std::cerr << "Unable to format serial data." << std::endl;
		}

		/***** Send command *****/
		if (serial_ != NULL) {
			Result result = serial_write(bufferCount, buffer, NULL, serial_);
			if(result < R_SUCCESS) {
				std::cerr << "Unable to send data." << std::endl;
				result = serial_destroy(const_cast<Serial**>(&serial_));
			} 
		}

		/***** Print command *****/
		std::cout << "\033[31m" << "Robot     : " << buffer << "\033[0m" << std::endl;
	}

	void Robot::Reverse(int const i_distance, int const i_angle) volatile {
		std::lock_guard<std::mutex> guard(const_cast<std::mutex&>(serial_mutex_));
		if(serial_ == NULL && Robot::Exists(kArduinoID)) {
			const_cast<Robot*>(this)->Set(kArduinoID);
		}

		/***** Clamp distance *****/
		int distance = i_distance;
		if(distance < 0) {
			distance = 0;
		} else if(distance > 999) {
			distance = 999;
		}

		/***** Clamp angle *****/
		int angle = i_angle;
		while(angle < 0) {
			angle += 360;
		}
		while(angle >= 360) {
			angle -= 360;
		}

		/***** Compose command *****/
		uint8_t buffer[8];
		int bufferCount = snprintf(reinterpret_cast<char *>(buffer), sizeof(buffer), "G%03d%03d", i_distance, i_angle);
		if(bufferCount <= 0) {
			std::cerr << "Unable to format serial data." << std::endl;
		}

		/***** Send command *****/
		if (serial_ != NULL) {
			Result result = serial_write(bufferCount, buffer, NULL, serial_);
			if(result < R_SUCCESS) {
				std::cerr << "Unable to send data." << std::endl;
				serial_destroy(const_cast<Serial**>(&serial_));
			}
		}

		/***** Print command *****/
		std::cout << "\033[31m" << "Robot     : " << buffer << "\033[0m" << std::endl;
	}

	void Robot::Stop() volatile {
		std::lock_guard<std::mutex> guard(const_cast<std::mutex&>(serial_mutex_));
		if(serial_ == NULL && Robot::Exists(kArduinoID)) {
			const_cast<Robot*>(this)->Set(kArduinoID);
		}

		if(serial_ != NULL) {
			uint8_t buffer[2];
			buffer[0] = ' ';
			buffer[1] = '\0';
			Result result = serial_write(1, buffer, NULL, serial_);
			if(result < R_SUCCESS) {
				std::cerr << "Unable to stop robot." << std::endl;
				serial_destroy(const_cast<Serial**>(&serial_));
			}
		}

		std::cout << "\033[31m" << "Robot     :" << "STOP" << "\033[0m" << std::endl;
	}

}
}
