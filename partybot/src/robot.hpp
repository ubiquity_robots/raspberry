// Author: Tuna Oezer (oezer@tunaoezer.com)
//
// Robot interface.

#ifndef UBIQUITY_ROBOTS_PARTYBOT_ROBOT_HPP_
#define UBIQUITY_ROBOTS_PARTYBOT_ROBOT_HPP_

#include "../tools/carl/Serial.h"

#include <mutex>

namespace ubiquity_robots {
namespace partybot {

	class Robot {
	 public:
		Robot();

		~Robot();

		// Must be called before Robot can be used.
		// Returns true if the Robot was successfully started.
		bool Init();

		// Distance: 0..999 centimeters.
		// Angle: 0..360 degrees, clockwise.
		void Forward(int speed, int angle) volatile;
		void GoTo(int distance, int angle) volatile;
		void Reverse(int speed, int angle) volatile;

		void Stop() volatile;

		static bool Exists(int const i_arduinoID);
	protected:
		bool Set(int const i_arduinoID);
	private:
		// Serial port.
		Serial* serial_;
		std::mutex serial_mutex_;
	};

}
}
#endif

