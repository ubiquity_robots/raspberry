// Author: Tuna Oezer (oezer@tunaoezer.com)
//
// Detects faces and body shapes in an image.

#ifndef UBIQUITY_ROBOTS_PARTYBOT_PERSON_DETECTOR_HPP_
#define UBIQUITY_ROBOTS_PARTYBOT_PERSON_DETECTOR_HPP_

#include <vector>

#include <opencv2/core/core.hpp>

#include "person.hpp"
#include "parameters.hpp"

namespace cv { class CascadeClassifier; }

namespace ubiquity_robots {
namespace partybot {

	class PersonDetector {
	 public:
		explicit PersonDetector(const Parameters& parameters);

		~PersonDetector();

		void Load();

		// Detects persons in an image.
		void Detect(cv::Mat const &image, PersonVector * const persons);

		const std::vector<cv::Rect>& faces() const {
			return faces_;
		}

		// Draws all results onto the image.
		void DrawAll(cv::Mat *io_image) const;

		// Draws rects onto the image.
		void Draw(const std::vector<cv::Rect>& rects,
				cv::Scalar color,
				cv::Mat *io_image) const;

	 private:
		void Rescale(std::vector<cv::Rect>* rects, int scale);

		const Parameters* parameters_;

		cv::CascadeClassifier* face_detector_;
		cv::CascadeClassifier* body_detector_;

		cv::Size min_face_size_;
		cv::Size min_body_size_;

		std::vector<cv::Rect> faces_;
		std::vector<cv::Rect> bodies_;
 };

}
}
#endif
