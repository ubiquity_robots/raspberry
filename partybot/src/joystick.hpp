// Author: Tuna Oezer (oezer@tunaoezer.com)
//
// Joystick controller.

#ifndef UBIQUITY_ROBOTS_PARTYBOT_JOYSTICK_HPP_
#define UBIQUITY_ROBOTS_PARTYBOT_JOYSTICK_HPP_

#include "behavior.hpp"
#include "parameters.hpp"
#include "robot.hpp"
#include "sound_player.hpp"

#include <thread>
extern "C" {
#include <linux/joystick.h>
}

namespace ubiquity_robots {
namespace partybot {

	class Joystick : protected std::thread {
	 public:
		explicit Joystick(Behavior volatile * const i_behavior, Robot volatile * const i_robot, const Parameters& parameters);

		// Initializes the Joystick.
		bool Init();
		void Run();
		bool Set(std::string const &i_joystickFilepath="");

		virtual ~Joystick();

		static bool Exists(std::string const &i_joystickFilepath);

	 protected:
		// Runs one iteration of the Joystick loop.
		bool Loop();

		void OnAxis(uint8_t const i_axis, int16_t const i_value);
		void OnButton(uint8_t const i_button, bool const i_down);

	 private:
		Behavior volatile *behavior_;

		uint8_t AXIS_GOTO_X;
		uint8_t AXIS_GOTO_Y;
		uint8_t AXIS_MANUAL_X;
		uint8_t AXIS_MANUAL_Y;
		uint8_t AXIS_TEST_LEFT;
		uint8_t AXIS_TEST_RIGHT;
		uint8_t BUTTON_DRIVE_BACKWARD;
		uint8_t BUTTON_DRIVE_FORWARD;
		uint8_t BUTTON_DRIVE_LEFT;
		uint8_t BUTTON_DRIVE_RIGHT;
		uint8_t BUTTON_DRIVE_STOP_0;
		uint8_t BUTTON_DRIVE_STOP_1;
		uint8_t BUTTON_EXIT;
		uint8_t BUTTON_MANUAL_TOGGLE_0;
		uint8_t BUTTON_MANUAL_TOGGLE_1;
		uint8_t BUTTON_MANUAL_ENABLE_0;
		uint8_t BUTTON_MANUAL_ENABLE_1;
		uint8_t BUTTON_MANUAL_DISABLE_0;
		uint8_t BUTTON_MANUAL_DISABLE_1;
		uint8_t BUTTON_PARTY;
		uint8_t BUTTON_VOICE_ROAM;
		uint8_t BUTTON_VOICE_TRACK;
		uint8_t BUTTON_VOICE_OFFER;
		uint8_t BUTTON_VOICE_DEPARTURE;

		struct js_event js_event_;
		int js_fd_;
		uint32_t js_features_;
		static size_t const js_name_length_max_ = 256;
		char js_name_[js_name_length_max_];
		uint32_t js_version_;
		uint8_t js_axis_count_;
		uint8_t js_button_count_;
		std::vector<int16_t> js_axes_;
		std::vector<int16_t> js_buttons_;
		int16_t js_rumble_id_;

		Parameters const * const parameters_;
		Robot volatile *robot_;
		SoundPlayer sound_player_;
	};

}
}
#endif
