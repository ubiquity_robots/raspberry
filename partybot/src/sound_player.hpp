// Author: Tuna Oezer (oezer@tunaoezer.com)
//
// Plays sounds.

#ifndef UBIQUITY_ROBOTS_PARTYBOT_SOUND_PLAYER_HPP_
#define UBIQUITY_ROBOTS_PARTYBOT_SOUND_PLAYER_HPP_

#include <vector>

namespace ubiquity_robots {
namespace partybot {

	class SoundPlayer {
	 public:

		// New sound files and types are added to sound_files.hpp.
		// If a new type is added, it must be also added to kSoundFiles in
		// sound_files.hpp.
		enum SoundTypes {
			ROAM_SOUND = 0,
			TRACK_SOUND = 1,
			DEPARTURE_SOUND = 2,
			OFFER_SOUND = 3,
			MUSIC_SOUND = 4,

			NUM_SOUND_TYPES	// Must be last.
		};

		SoundPlayer();

		// Returns the number of sound files for the specified type.
		static int NumSoundFiles(SoundTypes sound_type);

		// Plays a random sound of the specified type.
		void PlaySound(SoundTypes sound_type);

		// Plays the specified .wav file.
		void PlaySound(const char* filename);

	 private:
		// Number of sound files for each type.
		std::vector<int> num_sound_files_;

		// Current sound file index for each type.
		std::vector<int> sound_indices_;
	};

}
}

#endif
