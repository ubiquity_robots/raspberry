// Author: Tuna Oezer (oezer@tunaoezer.com)
//
// Controls the robot based on recognizer results.

#ifndef UBIQUITY_ROBOTS_VISION_DRIVER_HPP_
#define UBIQUITY_ROBOTS_VISION_DRIVER_HPP_

#include "../tools/carl/Serial.h"

namespace ubiquity_robots {
namespace vision {

class Driver {
 public:
  Driver();

  ~Driver();

  // Must be called before Driver can be used.
  // Returns true if the Driver was successfully started.
  bool Init(int image_width);

  void Drive(int face_center_x, int face_w);

 private:
  // Point at which robot will turn left.
  int left_boundary_;

  // Point at which robot will turn right.
  int right_boundary_;

  // Face width at which robot will stop.
  int stop_width_;

  // Serial port.
  Serial* serial_;

  Driver(const Driver& copy) { }
};

}
}

#endif
