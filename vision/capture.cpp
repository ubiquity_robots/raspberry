// Author: Tuna Oezer (oezer@tunaoezer.com)
//
// Implementation of Capture.

#include "capture.hpp"

#include <cstdio>
#include <fstream>
#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace std;

namespace ubiquity_robots {
namespace vision {

static const char kWindowName[] = "Capture";
static const int kMaxOutputFilePathLength = 4096;

void Capture::CaptureImages(const Parameters& parameters) {
  // Capture from first webcam.
  cv::VideoCapture video(parameters.camera());
  if (!video.isOpened()) {
    cout << "Error: Failed to open video." << endl;
    return;
  }
  ofstream label_file(parameters.label_file().c_str(),  ofstream::app);
  if (!label_file) {
    cout << "Error: Failed to open label file " << parameters.label_file() << endl;
    return;
  }
  int original_width = static_cast<int>(video.get(CV_CAP_PROP_FRAME_WIDTH));
  int original_height = static_cast<int>(video.get(CV_CAP_PROP_FRAME_HEIGHT));
  cout << "Original image width = " << original_width << endl;
  cout << "Original image height = " << original_height << endl;
  cout << "Output width = " << parameters.image_width() << endl;
  cout << "Output height = " << parameters.image_height() <<endl;
  cout << endl;
  cout << "Press a key to capture an image." << endl;
  cout << "Press 'q' to quit." << endl;
  bool downsample = parameters.image_width() < original_width ||
    parameters.image_height() < original_height;
  cv::namedWindow(kWindowName, CV_WINDOW_AUTOSIZE);
  cv::Mat frame;
  cv::Size output_size(parameters.image_width(), parameters.image_height());
  int image_number = parameters.data_start_index();
  char output_filepath[kMaxOutputFilePathLength];
  while (true) {
    video >> frame;
    cv::imshow(kWindowName, frame);
    int key = cv::waitKey(parameters.interval_ms());
    if (key == 'q') break;
    if (key > 0) {
      // Compute output file.
      sprintf(output_filepath, parameters.data_path().c_str(), image_number);
      // Convert to gray scale.
      cv::Mat gray_image;
      cv::cvtColor(frame, gray_image, CV_RGB2GRAY);
      // Down-sample if necessary.
      cv::Mat output_image;
      if (downsample) {
	cv::resize(gray_image, output_image, output_size);
      } else {
	output_image = gray_image;
      }
      // Save image.
      cv::imwrite(output_filepath, output_image);
      // Add line to label file with default label 0.
      label_file << output_filepath << kLabelFileDelimiter <<  "0"  << endl;
      cout << "Captured " << output_filepath << endl;
      image_number++;
    }
  }
  label_file.close();
}

}
}
