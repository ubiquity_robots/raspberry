// Author: Tuna Oezer (oezer@tunaoezer.com)
//
// Implements face recognition module.
// Supports 4 modes: capture, train, test apply.
// 1. Run with --mode=capture to capture a set of training images.
// 2a. Manually label the label file created in step 1 above using a text editor.
//     Replace the 0 after each file name with a positive number that represents the person.
// 2b. Run with --mode=train to train recognizer model using captured images.
// 2c. Test accuracy on training data with --mode=test
// 3. Run with --mode=recognize to run trained recognizer on streaming video.
// See PrintHelp() for parameter description.
// Parameters must be consistent in all 3 stages.

#include "capture.hpp"
#include "parameters.hpp"
#include "recognizer.hpp"
#include "training.hpp"
#include "tester.hpp"

#include <iostream>

void PrintHelp() {
  cout << "Face recognition module." << endl;
  cout << "Parameters: " << endl;
  cout << "--mode=capture\tCapture training images from video." << endl;
  cout << "--mode=train\tTrain recognizer model." << endl;
  cout << "--mode=test\tTest recognizer model." << endl;
  cout << "--mode=recognize\tRecognize in streaming video." << endl;
  cout << "--camera=<integer>\tIndex of video camera." << endl;
  cout << "--detector_model=<path>\tHaarcascade file to be used with detector." << endl;
  cout << "--detector_subsample=<integer>\tSubsampling factor for detector "
       << "(must be a power of 2)." << endl;
  cout << "--recognizer=eigen\tUse EigenFaces algorithm." << endl;
  cout << "--recognizer=fisher\tUse FisherFaces algorithm." << endl;
  cout << "--recognizer=lbph\tUse LBPH algorithm." << endl;
  cout << "--recognizer_model=<path>\tRecognizer model file (save or load)." << endl;
  cout << "--data=<path>*.<ext>\tPath used to capture training images with extension "
       << "(e.g., train_*.jpeg)" << endl;
  cout << "--start_index=<integer>\tStart index for data path wildcard to continue capture "
       << "after restart." << endl;
  cout << "--labels=<path>\tPath to text file with training image paths and labels, generated "
       << "by capture mode." << endl;
  cout << "--interval_ms=<integer>\tTime between two successive frames during"
       << " recognition." << endl;
  cout << "--w=<integer>\tImage width (original width/w must be multiple of 2)." << endl;
  cout << "--h=<integer>\tImage height (original height/h must be multiple of 2)." << endl;
  cout << "--face_w=<integer>\tCropped face width." << endl;
  cout << "--face_h=<integer>\tCropped face height." << endl;
  cout << "--min_size=<integer>\tMinimum object size in pixels." << endl;
  cout << "--min_confidence=<float>\tMinimum recognition confidence." << endl;
  cout << "--driver=off\tDriver mode is turned off (default)." << endl;
  cout << "--driver=detect\tDriver drives towards detected faces." << endl;
  cout << "--driver=recognize\tDriver drives towards recognized face with label "
       << "--driver_target." << endl;
  cout << "--driver_target=<integer>\tDriver target label if driver=recognize." << endl;
  cout << "--display=1\tDisplays recognition results in GUI in recognize mode." << endl;
}

int main(int argc, char** argv) {
  ubiquity_robots::vision::Parameters parameters;
  parameters.Init(argc, argv);
  switch (parameters.mode()) {
  case ubiquity_robots::vision::Parameters::CAPTURE:
    ubiquity_robots::vision::Capture::CaptureImages(parameters);
    break;
  case ubiquity_robots::vision::Parameters::TRAIN:
    ubiquity_robots::vision::Training::TrainRecognizer(parameters);
    break;
  case ubiquity_robots::vision::Parameters::TEST:
    ubiquity_robots::vision::Tester::Test(parameters);
    break;
  case ubiquity_robots::vision::Parameters::RECOGNIZE:
    ubiquity_robots::vision::Recognizer::Recognize(parameters);
    break;
  default: PrintHelp(); break;
  }
  return 0;
}
