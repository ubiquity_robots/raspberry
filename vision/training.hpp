// Author: Tuna Oezer (oezer@tunaoezer.com)
//
// Trains and saves a face recognition model.
// Uses the face recognizer specified in the parameters.
// Loads training images and label file specified in parameters.
// Label file must have been manually labeled before training is run.

#ifndef UBIQUITY_ROBOTS_VISION_TRAINING_HPP_
#define UBIQUITY_ROBOTS_VISION_TRAINING_HPP_

#include "parameters.hpp"

#include <string>
#include <vector>

namespace ubiquity_robots {
namespace vision {

class Training {
public:
  static void TrainRecognizer(const Parameters& parameters);
  static bool LoadLabelFile(const string& label_file_path,
			    vector<string>* image_files,
			    vector<int>* labels);
private:
  Training() { }
  Training(const Training& copy) { }
};

}
}

#endif
