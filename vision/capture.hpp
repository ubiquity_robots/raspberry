// Author: Tuna Oezer (oezer@tunaoezer.com)
//
// Captures images from video stream for training purposes.
// Captures one image with each key stroke.
// Displays video stream.

#ifndef UBIQUITY_ROBOTS_VISION_CAPTURE_HPP_
#define UBIQUITY_ROBOTS_VISION_CAPTURE_HPP_

#include "parameters.hpp"

namespace ubiquity_robots {
namespace vision {

class Capture {
public:
  static void CaptureImages(const Parameters& parameters);
	
private:
  Capture() { }
  Capture(const Capture& copy) { }
};

}
}

#endif
