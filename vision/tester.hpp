// Author: Tuna Oezer (oezer@tunaoezer.com)
//
// Tests face regonition accuracy.

#ifndef UBIQUITY_ROBOTS_VISION_TESTER_HPP_
#define UBIQUITY_ROBOTS_VISION_TESTER_HPP_

#include "parameters.hpp"

namespace ubiquity_robots {
namespace vision {

class Tester {
public:
  static void Test(const Parameters& parameters);

private:
  Tester() { }
  Tester(const Tester& copy) { }
};

}
}

#endif
