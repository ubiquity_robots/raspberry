// Author: Tuna Oezer (oezer@tunaoezer.com)
//
// Implementation of Training.

#include "training.hpp"

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include <opencv2/contrib/contrib.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>

namespace ubiquity_robots {
namespace vision {

bool Training::LoadLabelFile(const string& label_file_path,
			     vector<string>* image_files,
			     vector<int>* labels) {
  ifstream label_file(label_file_path.c_str());
  if (!label_file) {
    cout << "Failed to open label file " << label_file_path << endl;
    return false;
  }
  string line;
  while (label_file.good()) {
    getline(label_file, line);
    if (line.empty()) continue;
    size_t i = line.find(kLabelFileDelimiter);
    if (i == string::npos) {
      cout << "Warning (invalid label delimiter): " << line << endl;
      continue;
    }
    image_files->push_back(line.substr(0, i));
    stringstream conv;
    conv << line.substr(i + 1);
    int label = -1;
    conv >> label;
    if (label < 0) {
      cout << "Warning (invalid label): " << line << endl;
      label = 0;
    }
    labels->push_back(label);
  }
  label_file.close();
  return true;
}

// Extracts a face from an image, if it contains exactly one face.
// Skips images with no or multiple faces since labeling is ambiguous.
static bool ExtractFaces(const Parameters& parameters,
			 const vector<string>& image_files,
			 const vector<int>& loaded_labels,
			 vector<cv::Mat>* faces,
			 vector<int>* training_labels) {
  cv::CascadeClassifier classifier;
  if (!classifier.load(parameters.detector_model())) {
    cout << "Failed to open detector model file " << parameters.detector_model() << endl;
    return false;
  }
  cv::Mat image;
  cv::Size min_object_size(parameters.min_object_size(), parameters.min_object_size());
  cv::Size face_size(parameters.face_width(), parameters.face_height());
  for (int i = 0; i < image_files.size(); ++i) {
    cout << "Processing " << image_files[i] << "...";
    image = cv::imread(image_files[i], CV_LOAD_IMAGE_GRAYSCALE);
    // Normalize image.
    cv::equalizeHist(image, image);
    vector<cv::Rect> face_rects;
    classifier.detectMultiScale(
      image,
      face_rects,
      kDetectorScaleFactor,
      kDetectorMinNeighbors,
      CV_HAAR_SCALE_IMAGE,
      min_object_size);
    if (face_rects.size() == 1) {
      // Crop face image.
      cv::Mat face_image;
      cv::resize(image(face_rects[0]), face_image, face_size);
      faces->push_back(face_image);
      training_labels->push_back(loaded_labels[i]);
    } else {
      cout << "Error: " << face_rects.size() << " faces detected";
    }
    cout << endl;
  }
  return true;
}

static void Train(const Parameters& parameters,
		  const vector<cv::Mat>& faces,
		  const vector<int>& labels) {
  cv::Ptr<cv::FaceRecognizer> recognizer;
  switch (parameters.recognizer_algorithm()) {
    case Parameters::FISHER_FACES: recognizer = cv::createFisherFaceRecognizer(); break;
    case Parameters::LBPH_FACES: recognizer = cv::createLBPHFaceRecognizer(); break;
    default: recognizer = cv::createEigenFaceRecognizer(); break;
  }
  recognizer->train(faces, labels);
  recognizer->save(parameters.recognizer_model());
}

void Training::TrainRecognizer(const Parameters& parameters) {
  vector<string> image_files;
  vector<int> loaded_labels;
  if (!LoadLabelFile(parameters.label_file(), &image_files, &loaded_labels))
    return;
  cout << "Loaded " << loaded_labels.size() << " labels." << endl;
  vector<cv::Mat> faces;
  vector<int> training_labels;
  cout << "Extracting faces..." << endl;
  if (!ExtractFaces(parameters, image_files, loaded_labels, &faces, &training_labels))
    return;
  cout << "Extraced " << faces.size() << " faces." << endl;
  cout << "Training..." << endl;
  Train(parameters, faces, training_labels);
  cout << "Done." << endl;
}

}
}
