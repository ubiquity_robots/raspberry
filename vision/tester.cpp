// Author: Tuna Oezer (oezer@tunaoezer.com)
//
// Implementation of Tester.

#include "tester.hpp"

#include <iostream>
#include <opencv2/contrib/contrib.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>

#include "training.hpp"

namespace ubiquity_robots {
namespace vision {

static void TestFile(const Parameters& parameters,
		     const string& image_file,
		     int label,
		     cv::CascadeClassifier* detector,
		     const cv::FaceRecognizer* recognizer,
		     int* num_detected, int* num_recognized, int* num_recognized_strict) {
  cout << "Processing " << image_file << "..." << endl;
  cv::Mat image = cv::imread(image_file, CV_LOAD_IMAGE_GRAYSCALE);
  // Normalize image.
  cv::equalizeHist(image, image);
  cv::Size detector_image_size(image.cols / parameters.detector_subsample_factor(),
			       image.rows / parameters.detector_subsample_factor());
  cv::Mat sub_image;
  cv::resize(image, sub_image, detector_image_size);
  vector<cv::Rect> face_rects;
  detector->detectMultiScale(
      sub_image,
      face_rects,
      kDetectorScaleFactor,
      kDetectorMinNeighbors,
      CV_HAAR_SCALE_IMAGE,
      cv::Size(parameters.min_object_size(),
	       parameters.min_object_size()));
  if (face_rects.size() == 1)
    (*num_detected)++;
  cv::Size face_size(parameters.face_width(), parameters.face_height());
  for (int i = 0; i < face_rects.size(); ++i) {
    face_rects[i].x *= parameters.detector_subsample_factor();
    face_rects[i].y *= parameters.detector_subsample_factor();
    face_rects[i].width *= parameters.detector_subsample_factor();
    face_rects[i].height *= parameters.detector_subsample_factor();
    // Crop face image.
    cv::Mat face_image;
    cv::resize(image(face_rects[i]), face_image, face_size);
    // Recognize.
    int predicted_label;
    double confidence;
    recognizer->predict(face_image, predicted_label, confidence);
    cout << "\tFace " << i << ": label=" << predicted_label << " confidence=" << confidence << endl;
    if (confidence >= parameters.min_confidence()) {
      if (predicted_label == label) {
	(*num_recognized)++;
	if (face_rects.size() == 1)
	  (*num_recognized_strict)++;
      }
    }
  }
}

void Tester::Test(const Parameters& parameters) {
  // Load images and labels.
  vector<string> image_files;
  vector<int> labels;
  if (!Training::LoadLabelFile(parameters.label_file(), &image_files, &labels))
    return;
  cout << "Loaded " << labels.size() << " labels." << endl;
  // Load detector and recognizer models.
  cv::CascadeClassifier detector;
  if (!detector.load(parameters.detector_model())) {
    cout << "Failed to open detector model file " << parameters.detector_model() << endl;
    return;
  }
  cv::Ptr<cv::FaceRecognizer> recognizer;
  switch (parameters.recognizer_algorithm()) {
    case Parameters::FISHER_FACES: recognizer = cv::createFisherFaceRecognizer(); break;
    case Parameters::LBPH_FACES: recognizer = cv::createLBPHFaceRecognizer(); break;
    default: recognizer = cv::createEigenFaceRecognizer(); break;
  }
  recognizer->load(parameters.recognizer_model());
  int num_detected = 0;
  int num_recognized = 0;
  int num_recognized_strict = 0;
  for (int i = 0; i < image_files.size(); ++i) {
    TestFile(parameters, image_files[i], labels[i], &detector, recognizer.obj, &num_detected, &num_recognized, &num_recognized_strict);
  }
  cout << "Num detected: " << num_detected << " (" << (100.0 * num_detected / labels.size()) << "%)" << endl;
  cout << "Num recognized: " << num_recognized
       << " (" << (100.0 * num_recognized / labels.size()) << "%)"
       << " (" << (100.0 * num_recognized / num_detected) << "%)"
       << endl;
  cout << "Num recognized strict: " << num_recognized_strict
       << " (" << (100.0 * num_recognized_strict / labels.size()) << "%)"
       << " (" << (100.0 * num_recognized_strict / num_detected) << "%)"
       << endl;
}

}
}
