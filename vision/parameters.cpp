// Author: Tuna Oezer (oezer@tunaoezer.com)
//
// Implementation of Parameters.

#include "parameters.hpp"

#include <iostream>
#include <sstream>

namespace ubiquity_robots {
namespace vision {

static const char kMode[] = "mode";
static const char kCamera[] = "camera";
static const char kCapture[] = "capture";
static const char kTrain[] = "train";
static const char kTest[] = "test";
static const char kRecognize[] = "recognize";
static const char kDetectorModel[] = "detector_model";
static const char kRecognizerAlgorithm[] = "recognizer";
static const char kEigenFaces[] = "eigen";
static const char kFisherFaces[] = "fisher";
static const char kLBPH[] = "lbph";
static const char kRecognizerModel[] = "recognizer_model";
static const char kDataPath[] = "data";
static const char kDataStartIndex[] = "start_index";
static const char kLabelFile[] = "labels";
static const char kIntervalMs[] = "interval_ms";
static const char kImageWidth[] = "w";
static const char kImageHeight[] = "h";
static const char kDetectorSubsampleFactor[] = "detector_subsample";
static const char kFaceWidth[] = "face_w";
static const char kFaceHeight[] = "face_h";
static const char kMinObjectSize[] = "min_size";
static const char kMinConfidence[] = "min_confidence";
static const char kDriverMode[] = "driver";
static const char kDriverModeOff[] = "off";
static const char kDriverModeDetect[] = "detect";
static const char kDriverModeRecognize[] = "recognize";
static const char kDriverTargetLabel[] = "driver_target";
static const char kDisplay[] = "display";

Parameters::Parameters() {
	mode_ = UNDEFINED;
	camera_ = 0;
	detector_model_ = "/usr/local/share/OpenCV/haarcascades/haarcascade_frontalface_alt.xml";
	recognizer_algorithm_ = EIGEN_FACES;
	recognizer_model_ = "faces.xml";
	data_path_ = "train_%d.jpeg";
	data_start_index_ = 0;
	label_file_ = "labels.csv";
	interval_ms_ = 30;
	image_width_ = 640;
	image_height_ = 480;
	detector_subsample_factor_ = 2;
	face_width_ = 100;
	face_height_ = 100;
	min_object_size_ = 10;
	min_confidence_ = 0.0;
	driver_mode_ = DRIVER_OFF;
	driver_target_label_ = -1;
	display_ = true;
}

void Parameters::Init(int argc, char** argv) {
	for (int i = 1; i < argc; ++i) {
		string s(argv[i]);
		size_t j = s.find('=');
		if (j != string::npos) {
			string name = s.substr(2, j - 2);
			string value = s.substr(j + 1);
			if (name == kMode) {
				if (value == kCapture) {
					mode_ = CAPTURE;
				} else if (value == kTrain) {
					mode_ = TRAIN;
				} else if (value == kTest) {
					mode_ = TEST;
				} else if (value == kRecognize) {
					mode_ = RECOGNIZE;
				} else {
					cout << "Unknown mode " << value << endl;
				}
			} else if (name == kCamera) {
				stringstream conv;
				conv << value;
				conv >> camera_;
			} else if (name == kDetectorModel) {
				detector_model_ = value;
			} else if (name == kRecognizerAlgorithm) {
				if (value == kEigenFaces) {
					recognizer_algorithm_ = EIGEN_FACES;
				} else if (value == kFisherFaces) {
					recognizer_algorithm_ = FISHER_FACES;
				} else if (value == kLBPH) {
					recognizer_algorithm_ = LBPH_FACES;
				} else {
					cout << "Unknown recognizer algorithm " << value << endl;
				}
			} else if (name == kRecognizerModel) {
				recognizer_model_ = value;
			} else if (name == kDataPath) {
				data_path_ = value;
				j = data_path_.find('*');
				if (j != string::npos) {
					data_path_ = data_path_.replace(j, 1, "%d");
				} else {
					cout << "Warning: data path is missing wild card" << endl;
				}
			} else if (name == kDataStartIndex) {
				stringstream conv;
				conv << value;
				conv >> data_start_index_;
			} else if (name == kLabelFile) {
				label_file_ = value;
			} else if (name == kIntervalMs) {
				stringstream conv;
				conv << value;
				conv >> interval_ms_;
			} else if (name == kImageWidth) {
				stringstream conv;
				conv << value;
				conv >> image_width_;
			} else if (name == kImageHeight) {
				stringstream conv;
				conv << value;
				conv >> image_height_;
			} else if (name == kDetectorSubsampleFactor) {
				stringstream conv;
				conv << value;
				conv >> detector_subsample_factor_;
			} else if (name == kFaceWidth) {
				stringstream conv;
				conv << value;
				conv >> face_width_;
			} else if (name == kFaceHeight) {
				stringstream conv;
				conv << value;
				conv >> face_height_;
			} else if (name == kMinObjectSize) {
				stringstream conv;
				conv << value;
				conv >> min_object_size_;
			} else if (name == kMinConfidence) {
				stringstream conv;
				conv << value;
				conv >> min_confidence_;
			} else if (name == kDriverMode) {
				if (value == kDriverModeOff) {
					driver_mode_ = DRIVER_OFF;
				} else if (value == kDriverModeDetect) {
					driver_mode_ = DRIVER_DETECTED_FACE;
				} else if (value == kDriverModeRecognize) {
					driver_mode_ = DRIVER_RECOGNIZED_FACE;
				} else {
					cout << "Unknown driver mode " << value << endl;
				}
			} else if (name == kDriverTargetLabel) {
				stringstream conv;
				conv << value;
				conv >> driver_target_label_;
			} else if (name == kDisplay) {
				display_ = value == "1";
			} else {
				cout << "Unknown argument " << name << endl;
			}
		} else {
			cout << "Argument value is missing " << s << endl;
		}
	}
}

}
}
