// Author: Tuna Oezer (oezer@tunaoezer.com)
//
// Implementation of Driver.

#include "driver.hpp"

#include <iostream>
#include <unistd.h>

using namespace std;

namespace ubiquity_robots {
namespace vision {

// Robot stops if face covers this fraction of the image.
const double kStopWidthFraction = 0.5;
const unsigned char kForward = 'i';
const unsigned char kLeft = 'j';
const unsigned char kRight = 'k';
const unsigned char kStop = ' ';

Driver::Driver()
  : left_boundary_(0),
    right_boundary_(0),
    stop_width_(0),
    serial_(NULL) {
}

Driver::~Driver() {
  if (serial_ == NULL) return;
  Result result = R_FAILURE;
  result = serial_destroy(&serial_);
  if(result < R_SUCCESS) {
    cout << "Failed to destroy serial." << endl;
  }
}

bool Driver::Init(int image_width) {
  left_boundary_ = image_width / 3;
  right_boundary_ = image_width - left_boundary_;
  stop_width_ = static_cast<int>(image_width * kStopWidthFraction);
  Result result = R_FAILURE;
  result = serial_create(0, SERIAL_BAUDRATE_9600, SERIAL_MODE_ARDUINO, &serial_);
  if(result < R_SUCCESS) {
    cout << "Failed to initialize serial port." << endl;
    serial_ = NULL;
    return false;
  }
  cout << "Waiting for Arduino startup..." << endl;
  sleep(10);
  uint8_t read_buffer[7] = { 0 };
  result = serial_read(6, read_buffer, NULL, serial_);
  if(result < R_SUCCESS) {
    cout << "Failed to read ready." << endl;
    return false;
  }
  cout << "Arduino: " << read_buffer << endl;
  return true;
}

void Driver::Drive(int face_x, int face_w) {
  if (serial_ == NULL) return;
  unsigned char command = kStop;
  if (face_w < stop_width_) {
    if (face_x < left_boundary_) {
      command = kLeft;
    } else if (face_x > right_boundary_) {
      command = kRight;
    } else {
      command = kForward;
    }
  }
  Result result = R_FAILURE;
  result = serial_write(1, &command, NULL, serial_);
  if(result < R_SUCCESS) {
    cout << "Unable to send data." << endl;
  }
}

}
}
