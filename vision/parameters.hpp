// Author: Tuna Oezer (oezer@tunaoezer.com)
//
// Program parameters.

#ifndef UBIQUITY_ROBOTS_VISION_PARAMETERS_HPP_
#define UBIQUITY_ROBOTS_VISION_PARAMETERS_HPP_

#include <string>

using namespace std;

namespace ubiquity_robots {
namespace vision {

// Delimiter used in label file to separate file path from label.
const char kLabelFileDelimiter = ',';

// Detector parameter specifying how much the image size is reduced at each image scale.
const double kDetectorScaleFactor = 1.1;

// Detector parameter specifying how many neighbors each candidate rectangle should have to
// retain it.
const int kDetectorMinNeighbors = 3;

class Parameters {
public:
	// Program mode.
	enum Mode {
		UNDEFINED,  // unspecified
		CAPTURE,  // capture training images
		TRAIN,  // train from training images
		TEST,  // test result of training
		RECOGNIZE,  // detect and recognize in streaming video
	};

	// Face recognition algorithms.
	enum RecognizerAlgorithm {
		EIGEN_FACES,
		FISHER_FACES,
		LBPH_FACES,
	};

	// Controls the robot driver.
	enum DriverMode {
		DRIVER_OFF,  // No driver
		DRIVER_DETECTED_FACE,  // Drive towards a detected face
		DRIVER_RECOGNIZED_FACE  // Drive towards a recognized face, requires driver_target_label
	};

	Parameters();

	// Initialize from command line parameters.
	void Init(int argc, char** argv);

	// Program mode (see enum).
	const Mode& mode() const {
		return mode_;
	}

	// Index of video camera.
	const int camera() const {
		return camera_;
	}

	// Path to detector model file. Must be a Haar cascade.
	const string& detector_model() const {
		return detector_model_;
	}

	// Recognizer algorithm (see enum).
	const RecognizerAlgorithm& recognizer_algorithm() const {
		return recognizer_algorithm_;
	}

	// Path to recognizer model file.
	// Used for saving in training mode.
	// Used for loading in apply mode.
	const string& recognizer_model() const {
		return recognizer_model_;
	}

	// Path to training data images.
	// Used for saving during capture mode.
	// Contains '%d' wildcard.
	const string& data_path() const {
		return data_path_;
	}

	// Start index for wildcard in data path.
	// Allows continuing capture using same data prefix after a restart.
	int data_start_index() const {
		return data_start_index_;
	}

	// Path to text file with path to training images and associated data labels.
	// A label is a number >= 0 that uniquely identifies a person.
	// Each line of the file specifies a training image and its associated label delimited by
	// kLabelFileDelimiter.
	const string& label_file() const {
		return label_file_;
	}

	// During apply, recognizer will be applied every this many milliseconds.
	int interval_ms() const {
		return interval_ms_;
	}

	// Image width. Must be captured image width divided by multiple of 2.
	int image_width() const {
		return image_width_;
	}

	// Image height. Must be captured image height divided by multiple of 2.
	int image_height() const {
		return image_height_;
	}

	// Subsampling factor to be used for detection. Must be a power of 2 (e.g., 1, 2, 4, etc.).
	int detector_subsample_factor() const {
		return detector_subsample_factor_;
	}

	// Cropped face width.
	int face_width() const {
		return face_width_;
	}

	// Cropped face height.
	int face_height() const {
		return face_height_;
	}

	// Minimum object size in pixels. Smaller objects are ignored.
	int min_object_size() const {
		return min_object_size_;
	}

	// Minimum recognition confidence.
	double min_confidence() const {
		return min_confidence_;
	}

	const DriverMode& driver_mode() const {
		return driver_mode_;
	}

	int driver_target_label() const {
		return driver_target_label_;
	}

	// If true, recognition results are displayed in a window.
	bool display() const {
		return display_;
	}

private:
	Mode mode_;
	int camera_;
	string detector_model_;
	RecognizerAlgorithm recognizer_algorithm_;
	string recognizer_model_;
	string data_path_;
	int data_start_index_;
	string label_file_;
	int interval_ms_;
	int image_width_;
	int image_height_;
	int detector_subsample_factor_;
	int face_width_;
	int face_height_;
	int min_object_size_;
	double min_confidence_;
	DriverMode driver_mode_;
	int driver_target_label_;
	bool display_;

	Parameters(const Parameters& copy) {}
};

}
}
#endif