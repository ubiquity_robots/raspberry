// Author: Tuna Oezer (oezer@tunaoezer.com)
//
// Implementation of Recognizer.

#include "recognizer.hpp"

#include <iostream>
#include <opencv2/contrib/contrib.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>

#include "driver.hpp"

namespace ubiquity_robots {
namespace vision {

static const char kWindowName[] = "Recongizer";

void Recognizer::Recognize(const Parameters& parameters) {
  // Load detector and recognizer models.
  cv::CascadeClassifier classifier;
  if (!classifier.load(parameters.detector_model())) {
    cout << "Failed to open detector model file " << parameters.detector_model() << endl;
    return;
  }
  cv::Ptr<cv::FaceRecognizer> recognizer;
  switch (parameters.recognizer_algorithm()) {
    case Parameters::FISHER_FACES: recognizer = cv::createFisherFaceRecognizer(); break;
    case Parameters::LBPH_FACES: recognizer = cv::createLBPHFaceRecognizer(); break;
    default: recognizer = cv::createEigenFaceRecognizer(); break;
  }
  recognizer->load(parameters.recognizer_model());
  // Capture from first webcam.
  cv::VideoCapture video(parameters.camera());
  if (!video.isOpened()) {
    cout << "Error: Failed to open video." << endl;
    return;
  }
  int original_width = static_cast<int>(video.get(CV_CAP_PROP_FRAME_WIDTH));
  if (parameters.display()) {
    cout << "Press 'q' to quit." << endl;
  } else {
    cout << "Press 'ctr-c' to quit." << endl;
  }
  cv::Mat frame;
  cv::Size min_object_size(parameters.min_object_size(), parameters.min_object_size());
  cv::Size detector_image_size(parameters.image_width() / parameters.detector_subsample_factor(),
			       parameters.image_height() / parameters.detector_subsample_factor());
  cv::Size face_size(parameters.face_width(), parameters.face_height());
  if (parameters.display()) {
    cv::namedWindow(kWindowName, CV_WINDOW_AUTOSIZE);
  }
  cv::Scalar color_red(0, 0, 255);
  Driver driver;
  if (parameters.driver_mode() != Parameters::DRIVER_OFF) {
    driver.Init(original_width);
  }
  while (true) {
    video >> frame;
    // Convert to gray scale.
    cv::Mat gray_image;
    cv::cvtColor(frame, gray_image, CV_RGB2GRAY);
    // Normalize image.
    cv::equalizeHist(gray_image, gray_image);
    // Down-sample if necessary.
    cv::Mat detector_image;
    cv::resize(gray_image, detector_image, detector_image_size);
    vector<cv::Rect> face_rects;
    classifier.detectMultiScale(
        detector_image,
	face_rects,
	kDetectorScaleFactor,
	kDetectorMinNeighbors,
	CV_HAAR_SCALE_IMAGE,
	min_object_size);
    int best_face_index = -1;
    double best_confidence = 0.0;
    int best_face_center_x, best_face_width;
    for (int i = 0; i < face_rects.size(); ++i) {
      face_rects[i].x *= parameters.detector_subsample_factor();
      face_rects[i].y *= parameters.detector_subsample_factor();
      face_rects[i].width *= parameters.detector_subsample_factor();
      face_rects[i].height *= parameters.detector_subsample_factor();
      // Crop face image.
      cv::Mat face_image;
      cv::resize(gray_image(face_rects[i]), face_image, face_size);
      // Recongize.
      int label;
      double confidence;
      recognizer->predict(face_image, label, confidence);
      if (confidence > parameters.min_confidence()) {
	int half_width = static_cast<int>(face_rects[i].width * 0.5);
	int half_height = static_cast<int>(face_rects[i].height * 0.5);
	int face_center_x = face_rects[i].x + half_width;
	if (confidence > best_confidence) {
	  best_face_index = i;
	  best_face_center_x = face_center_x;
	  best_face_width = face_rects[i].width;
	}
	cout << "Recognized label=" << label << " with confidence=" << confidence
	     << " at x=" << face_rects[i].x << ", y=" << face_rects[i].y << endl;
	if (parameters.driver_mode() == Parameters::DRIVER_RECOGNIZED_FACE &&
	    label == parameters.driver_target_label()) {
	  driver.Drive(face_center_x, face_rects[i].width);
	}
	if (parameters.display()) {
	  // Indicate face in window.
	  cv::Point center(face_center_x, face_rects[i].y + half_height);
	  cv::ellipse(frame,
		      center,
		      cv::Size(half_width, half_height),   // axis
		      0,  // rotation
		      0,  // arc start
		      360,  // arc end
		      color_red,  // color
		      1,  // line thickness
		      8);  // line type
	}
      }
    }
    if (parameters.driver_mode() == Parameters::DRIVER_DETECTED_FACE &&
	best_face_index >= 0) {
      driver.Drive(best_face_center_x, best_face_width);
    }
    if (parameters.display()) {
      cv::imshow(kWindowName, frame);
      int key = cv::waitKey(parameters.interval_ms());
      if (key == 'q') break;
    }
  }
}

}
}
