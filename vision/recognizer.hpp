// Author: Tuna Oezer (oezer@tunaoezer.com)
//
// Recognizes faces in streaming video.
// Uses model trained by Training.

#ifndef UBIQUITY_ROBOTS_VISION_RECOGNIZER_HPP_
#define UBIQUITY_ROBOTS_VISION_RECOGNIZER_HPP_

#include "parameters.hpp"

namespace ubiquity_robots {
namespace vision {

class Recognizer {
public:
  static void Recognize(const Parameters& parameters);

private:
  Recognizer() { }
  Recognizer(const Recognizer& copy) { }
};

}
}

#endif
