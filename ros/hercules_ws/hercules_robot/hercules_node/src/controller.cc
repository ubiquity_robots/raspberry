#include <string>
#include <iostream>
#include <cstdio>

#include "ros/ros.h"
#include "geometry_msgs/Twist.h"
#include "geometry_msgs/Vector3.h"

#include "HerculesController.h"

using std::string;
using std::exception;
using std::cout;
using std::cerr;
using std::endl;

int main(int argc, char **argv){
	ros::init(argc, argv, "base_controller");
	HerculesController hc(argc, argv);
	ros::spin();
}
