#include <string>
#include <iostream>
#include <cstdio>
#include <unistd.h>

#include "ros/ros.h"
#include "geometry_msgs/Twist.h"
#include "geometry_msgs/Vector3.h"
#include "serial/serial.h"

class HerculesController{
   public:
      HerculesController(int argc, char **argv);
   private:
      //used to find velocities of each wheel 
      float rightVel(const geometry_msgs::Twist & movement);
      float leftVel(const geometry_msgs::Twist & movement);

      //callback for cmd_vel
      void moveCallback(const geometry_msgs::Twist::ConstPtr & movement);
      void move(int16_t left, int16_t right);

      ros::NodeHandle nh_;
      ros::Subscriber vel_sub_;

      static const float wheel_base = 0.6096;
      static const float wheel_diameter = 0.254;
      std::stringstream l;
      std::stringstream r;
};
