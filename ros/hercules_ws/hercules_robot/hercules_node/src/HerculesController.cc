#include "HerculesController.h"
#include <math.h>
#include <iomanip>
using std::string;
using std::exception;
using std::cout;
using std::cerr;
using std::endl;


serial::Serial my_serial(string("/dev/ttyACM0"),115200 );
HerculesController::HerculesController(int argc, char **argv){
   vel_sub_ = nh_.subscribe<geometry_msgs::Twist>
      ("/cmd_vel", 1, & HerculesController::moveCallback, this);
    cout << "Is the serial port open?";
  if(my_serial.isOpen())
    cout << " Yes." << endl;
  else
    cout << " No." << endl;
}
float HerculesController::rightVel(const geometry_msgs::Twist & movement){
   return (movement.linear.x + (movement.angular.z*wheel_base/2));
}

float HerculesController::leftVel(const geometry_msgs::Twist & movement){
   return (movement.linear.x - (movement.angular.z*wheel_base/2));
}

void HerculesController::moveCallback(const geometry_msgs::Twist::ConstPtr & movement){
   float r = rightVel(*movement);
   float l = leftVel(*movement);
   cout << "Right Wheel: " << r << endl;
   cout << "Left Wheel: " << l << endl;
   move(round(l * 1000),round( r * 1000));
}

void HerculesController::move(int16_t left,int16_t right){
   l << left;
   r << right;
   my_serial.write("M(" + l.str() + "," + r.str() + ")");
   cout << "M(" + l.str() + "," + r.str() +")" <<endl;
   l.str(""); 
   r.str(""); 
}

