// Author: Tuna Oezer (oezer@tunaoezer.com).
//
// Visualizes the result of vision algorithms by drawing indicators in the video stream.
//
// Usage: rosrun ubiquity_vision visualizer
// Input: camera/image, vision/faces
// Output: vision/image (image transport)
// Parameters: -

#include <vector>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "ros/ros.h"
#include "image_transport/image_transport.h"
#include "cv_bridge/cv_bridge.h"
#include "ubiquity_vision/RectList.h"

namespace ubiquity_robots {
namespace vision {

  const char kVideoStreamName[] = "camera/image";
  const char kFaceInputName[] = "vision/faces";
  const char kOutputName[] = "vision/image";

  const int kInputQueueSize = 1;
  const int kOutputQueueSize = 1;

  class Visualizer {
   public:
    explicit Visualizer(ros::NodeHandle* node)
      : video_stream_(*node) {
      publisher_ = video_stream_.advertise(kOutputName, kOutputQueueSize);
      face_sub_ = node->subscribe(kFaceInputName, kInputQueueSize, &Visualizer::FaceCallback, this);
      video_sub_ = video_stream_.subscribe(
          kVideoStreamName, kInputQueueSize, &Visualizer::VideoCallback, this);
    }

   private:
    void FaceCallback(const ubiquity_vision::RectListConstPtr& faces_msg) {
      std::vector<cv::Rect> faces;
      for (ubiquity_vision::RectList::_rects_type::const_iterator it = faces_msg->rects.begin();
	   it != faces_msg->rects.end();
	   ++it) {
	cv::Rect rect;
	rect.x = it->x;
	rect.y = it->y;
	rect.width = it->width;
	rect.height = it->height;
	faces.push_back(rect);
      }
      faces_.swap(faces);
    }

    void VideoCallback(const sensor_msgs::ImageConstPtr& video_msg) {
      cv_bridge::CvImagePtr cv_ptr;
      try {
	cv_ptr = cv_bridge::toCvCopy(video_msg);
      } catch (cv_bridge::Exception& e) {
	ROS_ERROR("cv_bridge exception: %s", e.what());
	return;
      }
      for (std::vector<cv::Rect>::const_iterator it = faces_.begin();
	   it != faces_.end();
	   ++it) {
	if (it->width == 0 || it->height == 0) continue;
	int half_width = it->width / 2;
	int half_height = it->height / 2;
	int face_center_x = it->x + half_width;
	cv::Point center(face_center_x, it->y + half_height);
	cv::ellipse(cv_ptr->image,
		    center,
		    cv::Size(half_width, half_height),   // axis
		    0,  // rotation
		    0,  // arc start
		    360,  // arc end
		    CV_RGB(255, 0, 0),  // color
		    1,  // line thickness
		    8);  // line type
      }
      publisher_.publish(cv_ptr->toImageMsg());
    }

    std::vector<cv::Rect> faces_;

    image_transport::ImageTransport video_stream_;
    image_transport::Subscriber video_sub_;
    ros::Subscriber face_sub_;
    image_transport::Publisher publisher_;

    Visualizer();
    Visualizer(const Visualizer&);
  };

}
}

int main(int argc, char** argv) {
  ros::init(argc, argv, "visualizer");
  ros::NodeHandle node;
  ubiquity_robots::vision::Visualizer visualizer(&node);
  ros::spin();
  return 0;
}
