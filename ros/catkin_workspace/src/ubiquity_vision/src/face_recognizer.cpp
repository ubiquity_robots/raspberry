// Author: Tuna Oezer (oezer@tunaoezer.com).
//
// Uses the output of the face detector to recognize faces in a video stream.
//
// Usage: rosrun ubiquity_vision face_recognizer
// Input: camera/image, vision/faces
// Output: vision/face_ids (RectIdList)
// Parameters: /ubiquitiy_vision/face_recognizer/algorithm (eigen, fisher, or lbph)
//             /ubiquitiy_vision/face_recognizer/faces_model
//             /ubiquitiy_vision/face_recognizer/face_width
//             /ubiquitiy_vision/face_recognizer/face_height

#include <opencv2/contrib/contrib.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <string>

#include "ros/ros.h"
#include "image_transport/image_transport.h"
#include "image_transport/subscriber_filter.h"
#include "message_filters/subscriber.h"
#include "message_filters/synchronizer.h"
#include "message_filters/sync_policies/approximate_time.h"
#include "cv_bridge/cv_bridge.h"
#include "ubiquity_vision/RectList.h"
#include "ubiquity_vision/RectIdList.h"

namespace ubiquity_robots {
namespace vision {

  const char kVideoStreamName[] = "camera/image";
  const char kFaceInputName[] = "vision/faces";
  const char kOutputName[] = "vision/face_ids";

  const char kAlgorithmParam[] = "/ubiquitiy_vision/face_recognizer/algorithm";
  const std::string kAlgorithm_Eigen("eigen");
  const std::string kAlgorithm_Fisher("fisher");
  const std::string kAlgorithm_LBPH("lbph");
  const std::string kDefaultAlgorithm(kAlgorithm_Eigen);

  const char kFaceModelParam[] = "/ubiquitiy_vision/face_recognizer/model";
  const char kDefaultFaceModel[] = "faces.xml";

  const char kFaceWidthParam[] = "/ubiquitiy_vision/face_recognizer/face_width";
  const int kDefaultFaceWidth = 320;
  const char kFaceHeightParam[] = "/ubiquitiy_vision/face_recognizer/face_height";
  const int kDefaultFaceHeight = 240;

  const int kInputQueueSize = 3;
  const int kSynchronizerQueueSize = 5;
  const int kOutputQueueSize = 1;

  class FaceRecognizer {
   public:
    explicit FaceRecognizer(ros::NodeHandle* node)
      : video_stream_(*node),
	video_filter_(video_stream_, kVideoStreamName, kInputQueueSize),
	faces_sub_(*node, kFaceInputName, kInputQueueSize),
	synchronizer_(TSyncPolicy(kSynchronizerQueueSize), video_filter_, faces_sub_) {
      std::string algorithm_name;
      node->param<std::string>(kAlgorithmParam, algorithm_name, kDefaultAlgorithm);
      if (algorithm_name == kAlgorithm_LBPH) {
	recognizer_ = cv::createLBPHFaceRecognizer();
      } else if (algorithm_name == kAlgorithm_Fisher) {
	recognizer_ = cv::createFisherFaceRecognizer();
      } else {  // assume kAlgorithm_Eigen
	recognizer_ = cv::createEigenFaceRecognizer();
      }
      std::string face_model_file;
      node->param<std::string>(kFaceModelParam, face_model_file, kDefaultFaceModel);
      recognizer_->load(face_model_file);

      int face_width, face_height;
      node->param<int>(kFaceWidthParam, face_width, kDefaultFaceWidth);
      node->param<int>(kFaceHeightParam, face_height, kDefaultFaceHeight);
      face_size_ = cv::Size(face_width, face_height);
      
      face_ids_pub_ = node->advertise<ubiquity_vision::RectIdList>(kOutputName, kOutputQueueSize);
      synchronizer_.registerCallback(boost::bind(&FaceRecognizer::MessageCallback, this, _1, _2));
    }

   private:
    typedef message_filters::sync_policies::ApproximateTime<
     sensor_msgs::Image, ubiquity_vision::RectList> TSyncPolicy;

    void MessageCallback(const sensor_msgs::ImageConstPtr& video_msg,
			 const ubiquity_vision::RectListConstPtr& faces_msg) {
      cv_bridge::CvImageConstPtr cv_ptr;
      try {
	cv_ptr = cv_bridge::toCvShare(video_msg);
      } catch (cv_bridge::Exception& e) {
	ROS_ERROR("cv_bridge exception: %s", e.what());
	return;
      }

      // Convert to gray scale.
      cv::Mat gray_image;
      cv::cvtColor(cv_ptr->image, gray_image, CV_RGB2GRAY);
      // Normalize image.
      cv::equalizeHist(gray_image, gray_image);

      ubiquity_vision::RectIdList result;
      for (ubiquity_vision::RectList::_rects_type::const_iterator it = faces_msg->rects.begin();
	   it != faces_msg->rects.end();
	   ++it) {
	cv::Rect rect;
	rect.x = it->x;
	rect.y = it->y;
	rect.width = it->width;
	rect.height = it->height;
	// Crop face image.
	cv::Mat face_image;
	cv::resize(gray_image(rect), face_image, face_size_);
	// Recongize.
	int label;
	double confidence;
	recognizer_->predict(face_image, label, confidence);

	ubiquity_vision::RectId rect_id;
	rect_id.rect.x = it->x;
	rect_id.rect.y = it->y;
	rect_id.rect.width = it->width;
	rect_id.rect.height = it->height;
	rect_id.id = label;
	rect_id.confidence = confidence;
	result.rect_ids.push_back(rect_id);
      }
      face_ids_pub_.publish(result);
    }

    cv::Ptr<cv::FaceRecognizer> recognizer_;
    cv::Size face_size_;
    image_transport::ImageTransport video_stream_;
    image_transport::SubscriberFilter video_filter_;
    message_filters::Subscriber<ubiquity_vision::RectList> faces_sub_;
    message_filters::Synchronizer<TSyncPolicy> synchronizer_;
    ros::Publisher face_ids_pub_;

    FaceRecognizer();
    FaceRecognizer(const FaceRecognizer&);
  };

}
}

int main(int argc, char** argv) {
  ros::init(argc, argv, "face_recognizer");
  ros::NodeHandle node;
  ubiquity_robots::vision::FaceRecognizer recognizer(&node);
  ros::spin();
  return 0;
}
