// Author: Tuna Oezer (oezer@tunaoezer.com).
//
// Detects faces in a video stream and publishes an array of rectangle coordinates that contain
// a face.
//
// Usage: rosrun ubiquity_vision face_detector
// Input: camera/image
// Output: vision/faces (RectList)
// Parameters: /ubiquitiy_vision/face_detector/haarcascade
//             /ubiquitiy_vision/face_detector/image_width
//             /ubiquitiy_vision/face_detector/image_height
//             /ubiquitiy_vision/face_detector/min_object_width
//             /ubiquitiy_vision/face_detector/min_object_height

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <string>
#include <vector>

#include "ros/ros.h"
#include "image_transport/image_transport.h"
#include "cv_bridge/cv_bridge.h"
#include "ubiquity_vision/RectList.h"

namespace ubiquity_robots {
namespace vision {

  const char kVideoStreamName[] = "camera/image";
  const char kOutputName[] = "vision/faces";

  const char kHaarcascadeParam[] = "/ubiquitiy_vision/face_detector/haarcascade";
  const char kDefaultHaarcascade[] =
    "/usr/local/share/OpenCV/haarcascades/haarcascade_frontalface_alt2.xml";

  const char kImageWidthParam[] = "/ubiquitiy_vision/face_detector/image_width";
  const int kDefaultImageWidth = 320;
  const char kImageHeightParam[] = "/ubiquitiy_vision/face_detector/image_height";
  const int kDefaultImageHeight = 240;

  const char kMinObjectWidthParam[] = "/ubiquitiy_vision/face_detector/min_object_width";
  const int kDefaultMinObjectWidth = 10;
  const char kMinObjectHeightParam[] = "/ubiquitiy_vision/face_detector/min_object_height";
  const int kDefaultMinObjectHeight = 10;

  // Detector parameter specifying how much the image size is reduced at each image scale.
  const double kDetectorScaleFactor = 1.1;

  // Detector parameter specifying how many neighbors each candidate rectangle should have to
  // retain it.
  const int kDetectorMinNeighbors = 3;

  const int kOutputQueueSize = 1;

  class FaceDetector {
   public:
    explicit FaceDetector(ros::NodeHandle* node)
      : video_stream_(*node) {
      std::string haarcascade_file;
      node->param<std::string>(kHaarcascadeParam, haarcascade_file, kDefaultHaarcascade);
      if (!classifier_.load(haarcascade_file)) {
	ROS_ERROR("Failed to open detector model file %s.", haarcascade_file.c_str());
      }

      int image_width, image_height;
      node->param<int>(kImageWidthParam, image_width, kDefaultImageWidth);
      node->param<int>(kImageHeightParam, image_height, kDefaultImageHeight);
      image_size_ = cv::Size(image_width, image_height);

      int min_obj_width, min_obj_height;
      node->param<int>(kMinObjectWidthParam, min_obj_width, kDefaultMinObjectWidth);
      node->param<int>(kMinObjectHeightParam, min_obj_height, kDefaultMinObjectHeight);
      min_object_size_ = cv::Size(min_obj_width, min_obj_height);

      faces_pub_ = node->advertise<ubiquity_vision::RectList>(kOutputName, kOutputQueueSize);
      video_sub_ = video_stream_.subscribe(kVideoStreamName, 1, &FaceDetector::VideoCallback, this);
    }

   private:
    void VideoCallback(const sensor_msgs::ImageConstPtr& video_msg) {
      cv_bridge::CvImageConstPtr cv_ptr;
      try {
	cv_ptr = cv_bridge::toCvShare(video_msg);
      } catch (cv_bridge::Exception& e) {
	ROS_ERROR("cv_bridge exception: %s", e.what());
	return;
      }

      // Down-sample.
      float horz_scale = static_cast<float>(cv_ptr->image.cols) / image_size_.width;
      float vert_scale = static_cast<float>(cv_ptr->image.rows) / image_size_.height;
      cv::Mat sub_image;
      cv::resize(cv_ptr->image, sub_image, image_size_);
      // Convert to gray scale.
      cv::Mat gray_image;
      cv::cvtColor(sub_image, gray_image, CV_RGB2GRAY);
      // Normalize image.
      cv::equalizeHist(gray_image, gray_image);
      std::vector<cv::Rect> face_rects;
      classifier_.detectMultiScale(
          gray_image,
	  face_rects,
	  kDetectorScaleFactor,
	  kDetectorMinNeighbors,
	  CV_HAAR_SCALE_IMAGE,
	  min_object_size_);

      ubiquity_vision::RectList result;
      for (int i = 0; i < face_rects.size(); ++i) {
	ubiquity_vision::Rect out_rect;
	out_rect.x = static_cast<int>(face_rects[i].x * horz_scale);
	out_rect.y = static_cast<int>(face_rects[i].y * vert_scale);
	out_rect.width = static_cast<int>(face_rects[i].width * horz_scale);
	out_rect.height = static_cast<int>(face_rects[i].height * vert_scale);
	result.rects.push_back(out_rect);
      }
      faces_pub_.publish(result);
    }

    cv::CascadeClassifier classifier_;
    cv::Size image_size_;
    cv::Size min_object_size_;
    image_transport::ImageTransport video_stream_;
    image_transport::Subscriber video_sub_;
    ros::Publisher faces_pub_;

    FaceDetector();
    FaceDetector(const FaceDetector&);
  };

}
}

int main(int argc, char** argv) {
  ros::init(argc, argv, "face_detector");
  ros::NodeHandle node;
  ubiquity_robots::vision::FaceDetector face_detector(&node);
  ros::spin();
  return 0;
}
