// Author: Tuna Oezer (oezer@tunaoezer.com).
//
// Captures video from a webcam via OpenCV and publishes the frames via ROS image transport.
//
// Usage: rosrun ubiquity_vision video_source
// Input: -
// Output: camera/image (image transport)
// Parameters: /camera/index, /camera/frame_rate

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "ros/ros.h"
#include "image_transport/image_transport.h"
#include "cv_bridge/cv_bridge.h"
#include "sensor_msgs/image_encodings.h"

const char kVideoStreamName[] = "camera/image";
const char kVideoCameraParam[] = "/camera/index";
const char kFrameRateParam[] = "/camera/frame_rate";
const int kDefaultVideoCamera = 0;
const int kDefaultFrameRate = 5;  // frames/second
const int kQueueSize = 1;

int main(int argc, char** argv) {
  ros::init(argc, argv, "video_source");
  ros::NodeHandle node;
  int video_camera;
  int frame_rate;
  node.param<int>(kVideoCameraParam, video_camera, kDefaultVideoCamera);
  node.param<int>(kFrameRateParam, frame_rate, kDefaultFrameRate);

  image_transport::ImageTransport video_stream(node);
  image_transport::Publisher publisher =
    video_stream.advertise(kVideoStreamName, kQueueSize);
  cv::VideoCapture video(video_camera);
  cv_bridge::CvImage frame;
  frame.encoding = sensor_msgs::image_encodings::BGR8;
  ros::Rate frame_rate_governor(frame_rate);
  while (node.ok()) {
    video >> frame.image;
    publisher.publish(frame.toImageMsg());
    ros::spinOnce();
    frame_rate_governor.sleep();
  }
  return 0;
}
