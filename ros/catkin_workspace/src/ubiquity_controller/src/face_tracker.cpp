// Author: Tuna Oezer (oezer@tunaoezer.com).
//
// Uses the output of the face detector to track a face.
//
// Usage: rosrun ubiquity_controller face_tracker
// Input: vision/faces
// Output: control/command (Command)
// Parameters: -

#include "ros/ros.h"
#include "ubiquity_controller/Command.h"
#include "ubiquity_vision/RectList.h"

#include "commands.h"

namespace ubiquity_robots {
namespace controller {

  const char kInputName[] = "vision/faces";
  const int kInputQueueSize = 1;
  const char kOutputName[] = "control/command";
  const int kOutputQueueSize = 1;

  class FaceTracker {
   public:
    explicit FaceTracker(ros::NodeHandle* node) {
      command_pub_ = node->advertise<ubiquity_controller::Command>(kOutputName, kOutputQueueSize);
      faces_sub_ = node->subscribe(kInputName, kInputQueueSize, &FaceTracker::FaceCallback, this);
    }

   private:
    void FaceCallback(const ubiquity_vision::RectListConstPtr& faces_msg) {
      ubiquity_controller::Command command;
      if (faces_msg->rects.empty()) {
	command.command = COMMAND_STOP;
      } else {
	// Pick first face.
	// Assume image width is 640 pixels. Should retrieve parameter.
	int pos_x = (faces_msg->rects[0].x + faces_msg->rects[0].width / 2) / 213;
	switch (pos_x) {
          case 0: command.command = COMMAND_LEFT; break;
          case 1: command.command = COMMAND_FORWARD; break;
          case 2: command.command = COMMAND_RIGHT; break;
	}
      }
      command_pub_.publish(command);
    }

    ros::Subscriber faces_sub_;
    ros::Publisher command_pub_;

    FaceTracker();
    FaceTracker(const FaceTracker&);
  };

}
}

int main(int argc, char** argv) {
  ros::init(argc, argv, "face_tracker");
  ros::NodeHandle node;
  ubiquity_robots::controller::FaceTracker face_tracker(&node);
  ros::spin();
  return 0;
}
