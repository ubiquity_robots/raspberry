// Author: Tuna Oezer (oezer@tunaoezer.com).
//
// Command constants.

namespace ubiquity_robots {
namespace controller {

  const int COMMAND_STOP = 0;
  const int COMMAND_FORWARD = 1;
  const int COMMAND_LEFT = 2;
  const int COMMAND_RIGHT = 3;

}
}
