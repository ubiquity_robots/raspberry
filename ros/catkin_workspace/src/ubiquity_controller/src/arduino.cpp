// Author: Tuna Oezer (oezer@tunaoezer.com).
//
// Sends commands to the Arduino.
//
// Usage: rosrun ubiquity_controller arduino
// Input: control/command
// Output: -
// Parameters: -

#include <unistd.h>

#include "ros/ros.h"
#include "ubiquity_controller/Command.h"
#include "commands.h"

#include "../../../../../tools/carl/Serial.h"

namespace ubiquity_robots {
namespace controller {

  const char kInputName[] = "control/command";
  const int kInputQueueSize = 10;

  const unsigned char kForward = 'i';
  const unsigned char kLeft = 'j';
  const unsigned char kRight = 'k';
  const unsigned char kStop = ' ';

  class Arduino {
   public:
    explicit Arduino(ros::NodeHandle* node)
      : serial_(NULL) {
      Result result = R_FAILURE;
      result = serial_create(0, SERIAL_BAUDRATE_9600, SERIAL_MODE_ARDUINO, &serial_);
      if(result < R_SUCCESS) {
	ROS_ERROR("Failed to initialize serial port.");
	return;
      }
      ROS_INFO("Waiting for Arduino startup...");
      sleep(10);
      uint8_t read_buffer[7] = { 0 };
      result = serial_read(6, read_buffer, NULL, serial_);
      if(result < R_SUCCESS) {
	ROS_ERROR("Failed to read ready.");
      }
      ROS_INFO("Arduino: %s", read_buffer);

      command_sub_ = node->subscribe(kInputName, kInputQueueSize, &Arduino::CommandCallback, this);
    }

    ~Arduino() {
      if (serial_ == NULL) return;
      Result result = R_FAILURE;
      result = serial_destroy(&serial_);
      if(result < R_SUCCESS) {
	ROS_ERROR("Failed to destroy serial.");
      }
    }

   private:
    void CommandCallback(const ubiquity_controller::CommandConstPtr& command_msg) {
      if (serial_ == NULL) return;
      unsigned char output = kStop;
      switch (command_msg->command) {
        case COMMAND_FORWARD: output = kForward; break;
        case COMMAND_LEFT: output = kLeft; break;
        case COMMAND_RIGHT: output = kRight; break;
      }
      ROS_INFO("Sending %c", output);
      Result result = R_FAILURE;
      result = serial_write(1, &output, NULL, serial_);
      if(result < R_SUCCESS) {
	ROS_ERROR("Unable to send data.");
      }
    }

    ros::Subscriber command_sub_;

    // Serial port.
    Serial* serial_;

    Arduino();
    Arduino(const Arduino&);
  };

}
}

int main(int argc, char** argv) {
  ros::init(argc, argv, "arduino");
  ros::NodeHandle node;
  ubiquity_robots::controller::Arduino arduino(&node);
  ros::spin();
  return 0;
}
