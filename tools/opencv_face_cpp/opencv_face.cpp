/**
 * @file objectDetection2.cpp
 * @author A. Huaman ( based in the classic facedetect.cpp in samples/c )
 * @brief A simplified version of facedetect.cpp, show how to load a cascade classifier and how to find objects (Face + eyes) in a video stream - Using LBP here
 */
#define GRAPHICAL
#include "carl/Camera.h"

#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <iostream>
#include <stdio.h>

/** Global variables */
cv::String const g_faceCascadeFilepath = "lbpcascade_frontalface.xml";
cv::String const g_eyeCascadeFilepath = "haarcascade_eye_tree_eyeglasses.xml";
cv::CascadeClassifier faceCascade;
cv::CascadeClassifier eyeCascade;

#ifdef GRAPHICAL
	cv::String const g_windowName = "Capture - Face detection";
#endif

/**
 * @function detectAndDisplay
 */
void detectAndDisplay( cv::Mat const &i_frame )
{
	std::vector<cv::Rect> faces;
	cv::Mat frameGray = i_frame;

	//equalizeHist( i_frame, frameGray );

	//-- Detect faces
	faceCascade.detectMultiScale( frameGray, faces, 1.1, 2, 0, cv::Size(80, 80) );

	for( size_t i = 0; i < faces.size(); i++ )
	{
#ifdef GRAPHICAL
		try
		{
			cv::Mat faceROI = frameGray( faces[i] );
			std::vector<cv::Rect> eyes;

			//-- In each face, detect eyes
			eyeCascade.detectMultiScale( faceROI, eyes, 1.1, 2, 0 |CV_HAAR_SCALE_IMAGE, cv::Size(30, 30) );
			//if( eyes.size() == 2)
			{
			 //-- Draw the face
			 cv::Point center( faces[i].x + faces[i].width/2, faces[i].y + faces[i].height/2 );
			 cv::ellipse( frameGray, center, cv::Size( faces[i].width/2, faces[i].height/2), 0, 0, 360, cv::Scalar( 255, 0, 0 ), 2, 8, 0 );

			 for( size_t j = 0; j < eyes.size(); j++ )
			  { //-- Draw the eyes
				 cv::Point eye_center( faces[i].x + eyes[j].x + eyes[j].width/2, faces[i].y + eyes[j].y + eyes[j].height/2 );
				 int radius = cvRound( (eyes[j].width + eyes[j].height)*0.25 );
				 cv::circle( frameGray, eye_center, radius, cv::Scalar( 255, 0, 255 ), 3, 8, 0 );
			  }
			}
		}
		catch(cv::Exception const &exc)
		{
		}
#else
		std::cout << faces[i] << std::endl;
#endif
	}

#ifdef GRAPHICAL
	//-- Show what you got
	imshow( g_windowName, frameGray );
#endif
}

void detectAndDrive(cv::Mat const &i_frame)
{
	std::vector<cv::Rect> faces;

	faceCascade.detectMultiScale( i_frame, faces, 1.1, 2, 0, cv::Size(80, 80) );
	for(size_t i=0; i<faces.size(); ++i)
	{
		std::cout << faces[i] << std::endl;
	}
	std::cout << "---" << std::endl;
}

void myCallback(uint8_t const * const i_frameData, size_t const i_frameSizeBytes, void *i_callbackData)
{
	//fprintf(stdout, "FRAME COPY (%p, %zu, %p)\n", i_frameData, i_frameSizeBytes, i_callbackData);
	//fflush(stdout);
	size_t i=0;
	size_t const frameYCount = i_frameSizeBytes/2;

	for(i=0; i<frameYCount; ++i)
	{
		((uint8_t*)i_callbackData)[i] = i_frameData[2*i];
	}
}

int const RX = 640;
int const RY = 480;

/**
 * @function main
 */
int main( void )
{
	if(!faceCascade.load(g_faceCascadeFilepath))
	{
		std::cerr << "Error loading '" << g_faceCascadeFilepath << "'" << std::endl;

		return 1;
	}

	if(!eyeCascade.load(g_eyeCascadeFilepath))
	{
		std::cerr << "Error loading '" << g_eyeCascadeFilepath << "'" << std::endl;
	}

	CameraHandle *cameraHandle=NULL;
	camera_create(0, CAMERA_PIXELFORMAT_YUYV, RX, RY, &cameraHandle);
	camera_start(cameraHandle);

	cv::Mat frame(RY, RX, CV_8UC1, 255);

	for(int i=0;i<100;++i)
	{
		camera_capture_callback(myCallback, frame.ptr(), cameraHandle);
		detectAndDrive(frame);
		//detectAndDisplay(frame);
		//imshow(g_windowName, frame);

		if(((char)cv::waitKey(10)) == 'q')
		{
			break;
		}
	}

	camera_stop(cameraHandle);
	camera_destroy(&cameraHandle);

	return 0;
}
