#!/usr/bin/python3
import termios, fcntl, serial, sys, os

### Get sounds ###
g_soundsDirectory = sys.argv[1]
g_sounds = sorted(os.listdir(g_soundsDirectory))

### Get Arduino ###
g_arduinoPort = serial.Serial('/dev/ttyUSB0', 9600, timeout=1)

### Print controls ###
os.system('clear')
print('		W - Forward')
print('A - Left			S - Right')
print('		D - Backward')
print('')
print('\tSOUNDS')
for idx in range(len(g_sounds)):
	print(str(idx)+'\t-\t'+g_sounds[idx])
print('')

### Get console ###
terminal = sys.stdin.fileno()

### Turn off echo and canon ###
terminalAttributesOld = termios.tcgetattr(terminal)
terminalAttributesNew = terminalAttributesOld
terminalAttributesNew[3] = terminalAttributesNew[3] & ~termios.ICANON & ~termios.ECHO
termios.tcsetattr(terminal, termios.TCSANOW, terminalAttributesNew)

### Set nonblock ###
terminalFlagsOld = fcntl.fcntl(terminal, fcntl.F_GETFL)
fcntl.fcntl(terminal, fcntl.F_SETFL, terminalFlagsOld | os.O_NONBLOCK)

g_soundString = ''
try:
	while 1:
		try:
			c = sys.stdin.read(1)
			if c=='w' or c=='a' or c=='s' or c=='d' or c==' ':
				g_arduinoPort.write(bytes(c, 'ascii'))
			elif c.isdigit():
				g_soundString += c
			elif c=='\r' or c=='\n':
				if len(g_soundString) > 0:
					os.system('aplay -q %s' % (g_soundsDirectory + '/' + g_sounds[int(g_soundString)]))
					g_soundString = ''
			elif c=='Q':
				break
		except IOError: pass
finally:
	termios.tcsetattr(terminal, termios.TCSAFLUSH, terminalAttributesOld)
	fcntl.fcntl(terminal, fcntl.F_SETFL, terminalFlagsOld)
	g_arduinoPort.close()

