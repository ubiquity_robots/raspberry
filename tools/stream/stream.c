#include "carl/Camera.h"
#include "carl/Timer.h"

#include <errno.h>
#include <stdio.h>
#include <sched.h>
#include <unistd.h>

char const * g_programName = "stream";

static int const TEST_FRAME_COUNT = 100;

int main(int const argc, char const * const * const argv)
{
	Result cameraResult = R_FAILURE;
	CameraHandle * cameraHandle = NULL;
	long coreCount = 0;
	cpu_set_t cpuMask;
	int i=0;
	unsigned char *myBuffer = NULL;
	struct sched_param priorityParam;
	int schedResult = -1;
	Timer timer;
	
	if(argc > 0)
	{
		g_programName = argv[0];
	}

	/***** Set affinity *****/
	coreCount = sysconf(_SC_NPROCESSORS_CONF);
	if(coreCount < 0)
	{
		fprintf(stderr, "%s: Unable to get number of cores - %s", g_programName, strerror(errno));
		goto end;
	}
	CPU_ZERO(&cpuMask);
	CPU_SET(coreCount-1, &cpuMask);
	schedResult = sched_setaffinity(0, sizeof(cpuMask), &cpuMask);
	if(schedResult < 0)
	{
		fprintf(stderr, "%s: Failed to set CPU affinity - %s", g_programName, strerror(errno));
		goto end;
	}

	/***** Set highest priority *****/
	priorityParam.sched_priority = sched_get_priority_max(SCHED_FIFO);
	schedResult = sched_setscheduler(0, SCHED_FIFO, &priorityParam);
	if(schedResult < 0)
	{
		fprintf(stderr, "%s: Failed to set FIFO scheduler - %s", g_programName, strerror(errno));
		goto end;
	}

	cameraResult = camera_create(	0,
											CAMERA_PIXELFORMAT_YUYV,
											640,
											480,
											&cameraHandle);
	if(cameraResult < R_SUCCESS)
	{
		fprintf(stderr, "%s: Failed to initialize camera.\n", g_programName);
		goto end;
	}

	cameraResult = camera_start(cameraHandle);
	if(cameraResult < R_SUCCESS)
	{
		fprintf(stderr, "%s: Failed to start camera.\n", g_programName);
		goto end;
	}

	fprintf(stdout, "Beginning capture of %d frames...\n", TEST_FRAME_COUNT);
	fflush(stdout);
	timer_start(&timer);
	for(i=0; i<TEST_FRAME_COUNT; ++i)
	{
		cameraResult = camera_capture_callback(NULL, NULL, cameraHandle);
	}
	timer_stop(&timer);

	fprintf(stdout, "Measured frametime: %g\n", timer_total_seconds(&timer)/((double)TEST_FRAME_COUNT));
	fprintf(stdout, "Measured FPS: %g\n", ((double)TEST_FRAME_COUNT)/timer_total_seconds(&timer));

	cameraResult = camera_stop(cameraHandle);
	if(cameraResult < R_SUCCESS)
	{
		fprintf(stderr, "%s: Failed to stop camera.\n", g_programName);
		goto end;
	}

	cameraResult = camera_destroy(&cameraHandle);
	if(cameraResult < R_SUCCESS)
	{
		fprintf(stderr, "%s: Failed to destroy camera.\n", g_programName);
		goto end;
	}

end:
	if(cameraHandle != NULL)
	{
		camera_destroy(&cameraHandle);
	}
	return 1;
}
