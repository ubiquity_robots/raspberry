#!/usr/bin/python
import argparse
import os
import serial
import textwrap
import time

SENSOR_NAMES = ['<', '^', '>', '/', '\\']
SENSOR_COUNT_MAX = len(SENSOR_NAMES)

#From http://stackoverflow.com/questions/517970/how-to-clear-python-interpreter-console
def cls():
	os.system('cls' if os.name == 'nt' else 'clear')

if __name__ == "__main__":
	##### Parse Arguments #####
	parser = argparse.ArgumentParser(description='Display XYZ sensor output')
	parser.add_argument('-D', '--device', action='store', nargs='?', default=(2 if os.name == 'nt' else '/dev/ttyUSB0'), help='The serial device (Linux) or serial number (Windows) that the XYZ sensor is on. [default=2]')
	parser.add_argument('-A', '--average', action='store', nargs='?', default=1.0, type=float, help='Number of seconds to average cycles over. [default=1.0]')
	args=parser.parse_args()
	
	##### Store Arguments #####
	CMDLINE_SENSOR_PORT = args.device
	CMDLINE_AVERAGE_DURATION = args.average
	
	##### Main Loop #####
	with serial.Serial(port=CMDLINE_SENSOR_PORT, baudrate=115200) as serialHandle:
		##### Initialize loop variables #####
		readingCounter=0
		timeLast=time.time()
		readings = [0] * SENSOR_COUNT_MAX
		cycleCount = 0
		
		##### Show initializing message #####
		cls()
		print('Initializing...')
		
		##### Avoid partial reading by waiting until start #####
		while serialHandle.isOpen():
			c=serialHandle.read(1)
			if c==b'\xfe':
				break
		
		while serialHandle.isOpen():
			c=serialHandle.read(1)
			if c==b'\xfe':
				##### Prepare for next cycle #####
				readingCounter = 0
				cycleCount += 1
				timeCurrent = time.time()
				
				##### Perhaps show results #####
				if (timeCurrent-timeLast) > CMDLINE_AVERAGE_DURATION:
					outputStringList = []
					
					##### Device Information #####
					outputStringList += ('Device Port:', str(CMDLINE_SENSOR_PORT))
					outputStringList += ('Averaged Duration:', str(CMDLINE_AVERAGE_DURATION) +' seconds')
					outputStringList += ('Averaged Cycles:', str(cycleCount) + ' cycles')
					
					##### Individual Sensor Information #####
					for i,reading in enumerate(readings):
						readingAveraged = (reading/cycleCount)
						bars = int(readingAveraged/2.55)
						outputStringList += ('Sensor '+str(i+1)+' ('+SENSOR_NAMES[i]+'):', '['+'{:.<100}'.format('='*bars)+'] '+str(round(readingAveraged)))
					
					##### Show Everything #####
					outputString = ''
					for i in range(0, len(outputStringList), 2):
						outputString += outputStringList[i].rjust(20)+' '+outputStringList[i+1]+'\n'
					cls()
					print(outputString)
					
					##### Reset relevant variables #####
					cycleCount = 0
					readings = [0]*SENSOR_COUNT_MAX
					timeLast = timeCurrent
			else:
				##### Store reading #####
				readings[readingCounter] += ord(c)
				readingCounter += 1

