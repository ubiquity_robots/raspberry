#!/bin/python
import termios, fcntl, sys, os
import socket

HOST='192.168.1.1'
PORT=3333

g_soundsPerseus = ['dalek-excuses.wav', 'dalek-exterminate.wav', 'dalek-iobey.wav', 'dalek-respect.wav', 'dalek-uexterminated.wav', 'dalek-underattk.wav', 'dalek-uprocede.wav']
g_soundsHercules = ['cyber-correct.wav', 'cyber-destroy.wav', 'cyber-die.wav', 'cyber-die2.wav', 'cyber-die3.wav', 'cyber-fire.wav', 'cyber-fire2.wav', 'cyber-impossible.wav', 'cyber-killthem.wav', 'cyber-madness.wav']

os.system('clear')
print('		W - Forward')
print('A - Left			S - Right')
print('		D - Backward')
print('')
print('\tHERCULES\t\t\tPERSEUS')
for idx in range(max([len(g_soundsHercules), len(g_soundsPerseus)])):
	if idx < len(g_soundsHercules) and idx < len(g_soundsPerseus):
		print(str(idx)+' -\t'+g_soundsHercules[idx]+'\t\t'+g_soundsPerseus[idx])
	elif idx < len(g_soundsHercules):
		print(str(idx)+' -\t'+g_soundsHercules[idx])
	elif idx < len(g_soundsPerseus):
		print(str(idx)+' -\t\t\t\t\t'+g_soundsHercules[idx])
print('')
print('==================== STATUS ====================')

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as remoteSocket:
	print('Connecting to '+HOST+':'+str(PORT)+'...')
	remoteSocket.connect((HOST,PORT))
	print('Connected.')

	print('Configuring input...')
	fd = sys.stdin.fileno()
	oldterm = termios.tcgetattr(fd)
	newattr = termios.tcgetattr(fd)
	newattr[3] = newattr[3] & ~termios.ICANON & ~termios.ECHO
	termios.tcsetattr(fd, termios.TCSANOW, newattr)
	print('Ready for input!')

	try:
		while 1:
			myChar = os.read(fd, 1)
			remoteSocket.sendall(myChar)
	finally:
		termios.tcsetattr(fd, termios.TCSAFLUSH, oldterm)
