#include "opencv2/opencv.hpp"

#include <iostream>
#include <sys/time.h>

static size_t const TEST_FRAME_COUNT = 100;

void prop(char const * const i_propertyName, int const i_propertyID, cv::VideoCapture &i_cap)
{
	std::cout << i_propertyName << ": " << i_cap.get(i_propertyID) << std::endl;
}

int main(int, char **)
{
	double timeTotal = 0.0;
	cv::Mat frame;
	size_t frameCount = 0;
	double measuredFPS = 0.0;
	struct timeval timeStop;
	struct timeval timeStart;

	cv::VideoCapture cap(0);
	if(!cap.isOpened())
	{
		return -1;
	}

	prop("Width", CV_CAP_PROP_FRAME_WIDTH, cap); 
	prop("Height", CV_CAP_PROP_FRAME_HEIGHT, cap);
	prop("FPS", CV_CAP_PROP_FPS, cap);
	prop("FOURCC", CV_CAP_PROP_FOURCC, cap);
	prop("RGB", CV_CAP_PROP_CONVERT_RGB, cap);

	std::cout << "Beginning capture of " << TEST_FRAME_COUNT << " frames..." << std::endl;
	gettimeofday(&timeStart, NULL);
	for(int i=0; i<TEST_FRAME_COUNT; ++i)
	{
		cap >> frame;
		++frameCount;
	}
	gettimeofday(&timeStop, NULL);

	timeTotal = (static_cast<double>(timeStop.tv_sec-timeStart.tv_sec)*1000000.0 + static_cast<double>(timeStop.tv_usec-timeStart.tv_usec))/1000000.0;
	measuredFPS = static_cast<double>(frameCount)/timeTotal;

	std::cout << "Measured FPS: " << measuredFPS << std::endl;

	return 0;
}
