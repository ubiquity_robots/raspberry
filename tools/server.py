import socket
import threading
import os
import serial
import sys

HOST = ''
PORT = 3333
ADDR = (HOST,PORT)
BUFSIZE = 4096

if len(sys.argv) > 1 and sys.argv[1] == 'perseus':
	g_sounds = ['dalek-excuses.wav', 'dalek-exterminate.wav', 'dalek-iobey.wav', 'dalek-respect.wav', 'dalek-uexterminated.wav', 'dalek-underattk.wav', 'dalek-uprocede.wav']
elif len(sys.argv) > 1 and sys.argv[1] == 'hercules':
	g_sounds = ['cyber-correct.wav', 'cyber-destroy.wav', 'cyber-die.wav', 'cyber-die2.wav', 'cyber-die3.wav', 'cyber-fire.wav', 'cyber-fire2.wav', 'cyber-impossible.wav', 'cyber-killthem.wav', 'cyber-madness.wav']
else:
	print('Use either "python server.py hercules" or "python server.py perseus"')
	exit(1)

class ClientSession(threading.Thread):
	def __init__(self,i_clientSocket,i_clientAddress):
		threading.Thread.__init__(self)
		self.clientSocket = i_clientSocket
		self.clientAddress = i_clientAddress
		self.size = 1024

	def run(self):
		running = 1
		while running:
			data = self.clientSocket.recv(1)
			if data:
				for item in data:
					itemChar=chr(item)
					if itemChar=='w' or itemChar=='a' or itemChar=='s' or itemChar=='d' or itemChar==' ':
						arduinoPort.write(bytes(itemChar,'ascii'))
					elif (item >= ord('0') and item <= ord('9')):
						os.system("aplay "+g_sounds[item-ord('0')])
					elif (item =='p'):
						self.clientSocket.close()
						running = 0
			else:
				self.clientSocket.close()
				running = 0
	
serverSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serverSocket.bind((HOST,PORT))
serverSocket.listen(5)

with serial.Serial('/dev/ttyUSB0', 9600, timeout=1) as arduinoPort:
	while 1:
		clientSocket,clientAddress = serverSocket.accept()
		clientThread = ClientSession(clientSocket, clientAddress)
		clientThread.run()
