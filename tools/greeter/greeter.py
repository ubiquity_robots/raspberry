#!/usr/bin/python
import smtplib
import socket

#Some constants
HERCULES_EMAIL_DOMAIN='gmail.com'
HERCULES_EMAIL_NAME='R. Hercules'
HERCULES_EMAIL_USERNAME='ubiquity.hercules'
HERCULES_EMAIL_PASSWORD='h3rcul3s'
HERCULES_EMAIL_SMTP_SERVER='smtp.gmail.com'
HERCULES_EMAIL_SMTP_PORT='587'

#Inspired by http://stackoverflow.com/questions/166506/finding-local-ip-addresses-using-pythons-stdlib
ipList = [ip for ip in socket.gethostbyname_ex(socket.gethostname())[2] if not ip.startswith("127.")][:1]

from email.mime.text import MIMEText

msg = MIMEText("Hi! This is Hercules!\n\nMy IP is " + ipList[0] + ".\n\nRegards,\n -Herc")
msg['Subject'] = 'Hercules online!'
msg['From'] = HERCULES_EMAIL_NAME+'<'+HERCULES_EMAIL_USERNAME+'@'+HERCULES_EMAIL_DOMAIN+'>'
msg['To'] = 'peasteven@gmail.com'

#Inspired by http://mynthon.net/howto/-/python/python%20-%20logging.SMTPHandler-how-to-use-gmail-smtp-server.txt
s=smtplib.SMTP(HERCULES_EMAIL_SMTP_SERVER, HERCULES_EMAIL_SMTP_PORT)
s.ehlo()
s.starttls()
s.ehlo()
s.login(HERCULES_EMAIL_USERNAME, HERCULES_EMAIL_PASSWORD)
s.sendmail(msg['From'], msg['To'], msg.as_string())
s.quit()

