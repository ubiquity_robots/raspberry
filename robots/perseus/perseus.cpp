#define GRAPHICAL
#include "carl/Camera.h"
#include "carl/Serial.h"

#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <iostream>
#include <stdio.h>
#include <unistd.h>

/** Global variables */
cv::String const g_faceCascadeFilepath = "lbpcascade_frontalface.xml";
cv::String const g_eyeCascadeFilepath = "haarcascade_eye_tree_eyeglasses.xml";
cv::CascadeClassifier faceCascade;
cv::CascadeClassifier eyeCascade;

void myCallback(uint8_t const * const i_frameData, size_t const i_frameSizeBytes, void *i_callbackData)
{
	size_t i=0;
	size_t const frameYCount = i_frameSizeBytes/2;

	for(i=0; i<frameYCount; ++i)
	{
		((uint8_t*)i_callbackData)[i] = i_frameData[2*i];
	}
}

int const RX = 640;
int const RY = 480;

int main( int const argc, char const * const * const argv)
{
	CameraHandle *cameraHandle = NULL;
	Serial *serialHandle = NULL;
	Result cameraResult = R_FAILURE;
	uint8_t readBuffer[7] = {0};
	Result serialResult = R_FAILURE;
	size_t i=0;
	std::vector<cv::Rect> faces;
	cv::Mat frame(RY, RX, CV_8UC1, 255);
	
	if(argc > 0)
	{
		g_programName = argv[0];
	}

	if(!faceCascade.load(g_faceCascadeFilepath))
	{
		fprintf(stderr, "%s: Error loading face data\n", g_programName);
		goto end;
	}

	if(!eyeCascade.load(g_eyeCascadeFilepath))
	{
		fprintf(stderr, "%s: Error loading eye data.\n", g_programName);
		goto end;
	}

	fprintf(stdout, "Initializing camera...\n");
	cameraResult = camera_create(	0,
											CAMERA_PIXELFORMAT_YUYV,
											RX,
											RY,
											&cameraHandle);
	if(cameraResult < R_SUCCESS)
	{
		fprintf(stderr, "%s: Failed to initialize camera.\n", g_programName);
		goto end;
	}

	fprintf(stdout, "Starting camera capture...\n");
	cameraResult = camera_start(cameraHandle);
	if(cameraResult < R_SUCCESS)
	{
		fprintf(stderr, "%s: Failed to start camera.\n", g_programName);
		goto end;
	}

	fprintf(stdout, "Initializing serial...\n");
	serialResult = serial_create(0, SERIAL_BAUDRATE_9600, SERIAL_MODE_ARDUINO, &serialHandle);
	if(serialResult < R_SUCCESS)
	{
		fprintf(stderr, "%s: Failed to initialize serial port.\n", g_programName);
		goto end;
	}

	fprintf(stdout, "Waiting for Arduino startup...\n");
	usleep(10000000);

	fprintf(stdout, "Waiting for Arduino ready...\n");
	serialResult = serial_read(6, readBuffer, NULL, serialHandle);
	if(serialResult < R_SUCCESS)
	{
		fprintf(stderr, "%s: Failed to read ready.\n", g_programName);
		goto end;
	}
	fprintf(stdout, "ARDUINO: %s\n", readBuffer);

	for(;;)
	{
		camera_capture_callback(myCallback, frame.ptr(), cameraHandle);
		faces.clear();

		faceCascade.detectMultiScale(frame, faces, 1.1, 2, 0, cv::Size(80, 80));

		if(faces.size() > 0)
		{
			for(i=0; i<faces.size(); ++i)
			{
				fprintf(stdout, "FACE: (%d, %d, %d, %d)\n", faces[i].x, faces[i].y, faces[i].width, faces[i].height);
			}

			double steering = static_cast<double>(faces[0].x + faces[0].width/2)/static_cast<double>(RX);
			fprintf(stdout, "STEER: %.2g\n", steering);

			if(steering < 0.45)
			{
				unsigned char const letterA = 'a';
				serialResult = serial_write(1, &letterA, NULL, serialHandle);
			}
			else if(steering > 0.55)
			{
				unsigned char const letterD = 'd';
				serialResult = serial_write(1, &letterD, NULL, serialHandle);
			}
			else
			{
				unsigned char const letterW = 'w';
				serialResult = serial_write(1, &letterW, NULL, serialHandle);
			}

			if(serialResult < R_SUCCESS)
			{
				fprintf(stderr, "%s: Unable to send data.\n", g_programName);
				goto end;
			}
		}
		else
		{
			fprintf(stdout, "---\n");
		}
	}

	serialResult = serial_destroy(&serialHandle);
	if(serialResult < R_SUCCESS)
	{
		fprintf(stderr, "%s: Failed to destroy serial.\n", g_programName);
		goto end;
	}

	cameraResult = camera_stop(cameraHandle);
	if(cameraResult < R_SUCCESS)
	{
		fprintf(stderr, "%s: Failed to stop camera.\n", g_programName);
		goto end;
	}

	cameraResult = camera_destroy(&cameraHandle);
	if(cameraResult < R_SUCCESS)
	{
		fprintf(stderr, "%s: Failed to destroy camera.\n", g_programName);
		goto end;
	}

	return 0;

end:
	if(serialHandle != NULL)
	{
		serial_destroy(&serialHandle);
	}

	if(cameraHandle != NULL)
	{
		camera_destroy(&cameraHandle);
	}

	return 1;
}

