#!/bin/bash
# Ubuntu 12.04 unattended ROS groovy binary install
#
# Status - works fine for installing ROS binaries on 12.04 LTS as of 01/12/2013
#
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu precise main" > /etc/apt/sources.list.d/ros-latest.list'

wget http://packages.ros.org/ros.key -O - | sudo apt-key add -

sudo apt-get update

export DEBIAN_FRONTEND=noninteractive
echo "hddtemp hddtemp/daemon boolean false" | sudo debconf-set-selections
sudo apt-get -y install ros-groovy-desktop-full

echo "source /opt/ros/groovy/setup.bash" >> ~/.bashrc
. ~/.bashrc

sudo apt-get install python-rosinstall python-rosdep

sudo rosdep init
rosdep update

