#!/bin/bash
if [ $# -ne 1 ]
then
  echo "Usage: ./launch.sh imagefilename (assumes you have kernel-qemu in this directory)"
  exit 1 
fi
sudo qemu-system-arm -kernel kernel-qemu -cpu arm1176 -m 256 -M versatilepb -net nic -net tap -no-reboot -append "root=/dev/sda2 panic=1" -hda $1 
