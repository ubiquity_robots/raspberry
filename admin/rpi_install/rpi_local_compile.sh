#!/bin/bash
# ROS groovy on Raspberry Pi wheezy - source install script
#
# Status: Although faithfully based on willow garage instructions, there is no way this will make it all the way through without heavy mods.
# The way to build an entire operating system for the RPi (which this is equivalent to) is to cross compile.
# If you are still interested in this script, it was tested through the collada compile step working, and it is based on these documents:
#
# http://www.ros.org/wiki/groovy/Installation/Raspbian/Source - primary source
# https://sites.google.com/site/ubiquityrobots/container-ship-of-projects/ros-install - robot club notes
# 
#

# wheezy binary packages
#
sudo apt-get update
sudo apt-get install openssh-server
sudo apt-get install build-essential python-yaml cmake subversion wget python-setuptools mercurial git-core
sudo apt-get install python-yaml libapr1-dev libaprutil1-dev  libbz2-dev python-dev libgtest-dev python-paramiko libboost-all-dev liblog4cxx10-dev pkg-config python-empy swig
sudo apt-get install libapr1-dev libaprutil1-dev libbz2-dev python-dev libgtest-dev python-paramiko libboost-all-dev liblog4cxx10-dev pkg-config python-empy swig python-nose lsb-release
sudo apt-get install  libtiff-dev libpoco-dev assimp-utils libtinyxml-dev python-pydot python-qwt5-qt4 libxml2-dev libtiff5-dev libsdl-image1.2-dev 
sudo apt-get install python-wxgtk2.8 python-gtk2 python-matplotlib libwxgtk2.8-dev python-imaging libqt4-dev graphviz qt4-qmake python-numpy
sudo apt-get install bison++ automake autoconf

# ros dependencies
#
sudo apt-get install python-pip python-dev build-essential 
sudo pip install --upgrade pip 
sudo pip install --upgrade virtualenv 
sudo pip install -U rosdep

# collada dom and tbb
#
sudo wget http://sourceforge.net/projects/collada-dom/files/latest/download
tar -xf download
cd collada-dom-*
cmake . && make install
cd ..
mkdir tbb
cd tbb
sudo wget http://threadingbuildingblocks.org/sites/default/files/software_releases/source/tbb41_20121112oss_src.tgz
tar -xf tbb41_20121112oss_src.tgz
cd tbb41_20121112oss
make

# wstool, rospkg, rosdep
#
sudo easy_install wstool
sudo easy_install rospkg
sudo easy_install rosdep 

# swig
#
$sudo apt-get install bison++ automake autoconf
git clone https://github.com/ros/swig-wx.git
cd swig-wx
./autogen.sh && ./configure && make && sudo make install

# rosdep
#
sudo mkdir /opt/ros
sudo mkdir /opt/ros/groovy
cd /opt/ros/groovy
sudo rosdep init
rosdep update

# catkin and desktop-full ros
#
sudo mkdir /opt/ros/groovy/ros_catkin_ws
cd /opt/ros/groovy/ros_catkin_ws
sudo wstool init -j1 src http://packages.ros.org/web/rosinstall/generate/raw/groovy/desktop-full
sudo rosdep install --from-paths src --ignore-src --rosdistro groovy -y
./src/catkin/bin/catkin_make_isolated --install
source /opt/ros/groovy/ros_catkin_ws/install_isolated/setup.bash

# rosbuild workspace
#
mkdir ~/ros_ws
cd ~/ros_ws
rosws init . ~/ros_catkin_ws/install_isolated
rosws init . /opt/ros/groovy/ros_catkin_ws/install_isolated
rosws merge http://packages.ros.org/web/rosinstall/generate/dry/raw/groovy/desktop-full
rosws update -j1
source ~/ros_ws/setup.bash
rosmake -a
