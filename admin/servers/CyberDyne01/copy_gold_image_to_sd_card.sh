#!/bin/bash
#
# CyberDyn01 image copier script
# richbodo@gmail.com
#

###
# yes no question function 
#
###
Verify_With_User ()
{
  echo -n $@
  read YnAnswer
  YnAnswer=$(echo $YnAnswer | awk '{print tolower($0)}')
  case $YnAnswer in
    "yes")  echo "Yesss!!!" ;;
    "no") exit 1 ;;
    *)      echo "Please answer yes or no" ; Verify_With_User;;
  esac
  echo
}

###
# understand the removable media situation
#
###
Check_For_Device ( )
{
  echo 
  echo
  echo "Remove any removable media you don't want to use now."
  echo "Plug in the removable media you DO want to use and hit ENTER..."
  read ENTER_KEY
  echo
  echo

  echo -n "Checking for partitions mounted on /media..."
  set -- $(mount | grep "media" | awk {'print $1'})
  mounted_media=$@
  echo $mounted_media

  echo -n "Checking for /dev/sdbX devices..."
  set -- $(sudo fdisk -l | grep /dev/sdb | awk {'print $1'})
  sdb_partitions=$@
  echo $sdb_partitions
  echo

  if [ -z "${sdb_partitions}" ]; then
      question="It doesn't look like a partition table exists for the media you have inserted.  Are you sure you want to continue (now would be a good time to check that your SD card is fully inserted, etc, before continuing) (yes/no)?"
      Verify_With_User $question
  fi
  echo
}

###
# entertain and inform while the image is copied
#
###
Jibba_Jabba ()
{
  echo
  echo "This will take 5 to 10 minutes for an 8G SD card - you will see a printout of progress in terms of MB copied starting in a minute or so."
  echo
  echo "While you are waiting, I might note that not all SD cards are the same size, exactly.  Although we have endeavored to make the gold image small enough to fit on any 8G SD card, don't panic if you see an output like:"
  echo "7424 blocks (7424Mb) written.dcfldd:: No space left on device"
  echo "What that means is that you have a slightly smaller SD card than usual, and you will have to resize one partition, eventually.  You should still be able to use the image as-is."
  echo "To resize that last partition, run fdisk on the card, delete the last partition, create a new partition that starts one sector after the other partitions, and ends at the last sector on the disk, set the correct partition type, and write it to the card.  It can be a little tricky to determine the last sector of the disk, sometimes.  Future versions of this script may fix that for you automatically."
  echo
}

###
# copy the image to removable media with appropriate checking
#
###
Copy_Image_To_Device ()
{
  date
  echo
  gold_image_link="/home/t1000/Desktop/GOLD.img"
  gold_image_file=$(readlink -f $gold_image_link)

  Jibba_Jabba
  
  if [ "${mounted_media}" ]; then
      echo "Unmounting media device partitions...$mounted_media"
      for mount_point in $mounted_media; do
	  CMD="sudo umount $mount_point"
	  $CMD
      done
  fi

  # verification would take way, way too long on this machine - 5 min or so
  # echo "Verifying gold image checksum on server..."
  # md5sum $gold_image_file
  # compare
 
  echo "Copying gold image to removable media..."
  CMD="sudo dcfldd bs=1M if=$gold_image_file of=/dev/sdb"
  $CMD
  
  # again, skipping automatic verification due to hardware constraints for the moment - takes even longer to verify an SD card
  # echo "Verifying image on removable media..."
  # compare
  echo
  date
  sync
  sync
  echo
  exit 0
}

###
# main
#
###
q1="This will wipe out the first removable media device plugged in (make sure there is only one), are you really sure you want to do that? (yes/no): "
q2="Last Chance.  Seriously.  Any removable media device plugged in will be erased forever and it's contents replaced with the "gold" image we are using for robot development.  NO BACKUP OF YOUR MEDIA WILL BE MADE.  Are you REALLY sure you want to wipe your media? (yes/no): "

echo "At some point, this script will ask you for the superuser[sudo] password for the logged in user.  It's the same one you used to login.  When it does, type that password and hit enter."
echo

Check_For_Device
Verify_With_User $q1
Verify_With_User $q2
Copy_Image_To_Device

echo "You may now remove your SD card - press any key to exit.  All done!"
read EXITKEY


